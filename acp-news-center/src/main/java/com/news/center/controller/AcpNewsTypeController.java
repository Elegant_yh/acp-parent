package com.news.center.controller;


import com.news.center.common.Result;
import com.news.center.common.ResultCode;
import com.news.center.dto.PageDto;
import com.news.center.entity.AcpNewsType;
import com.news.center.service.IAcpNewsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Controller
@RequestMapping("/newsType")
public class AcpNewsTypeController {

    @Autowired
    private IAcpNewsTypeService iAcpNewsTypeService;

    /**
     * 添加类型 或者 修改类型
     * @param acpNewsType
     * @return
     */
    @PostMapping("/saveOModifyNewsType")
    @ResponseBody
    public Result saveOModifyNewsType(@RequestBody AcpNewsType acpNewsType){
        if (acpNewsType.getNewsTypeId() != null && acpNewsType.getNewsTypeId() > 0)
            return iAcpNewsTypeService.modifyNewsType(acpNewsType);
        else
            return iAcpNewsTypeService.saveNewsType(acpNewsType);
    }

    /**
     * 删除
     * @param newsTypeId
     * @return
     */
    @DeleteMapping("/deleteNewsType/{newsTypeId}")
    @ResponseBody
    public Result deleteNewsType(@PathVariable("newsTypeId") Long newsTypeId){
        return iAcpNewsTypeService.deleteNewsType(newsTypeId);
    }

    /**
     * 查询所有
     * @param newsTypeName
     * @return
     */
    @PostMapping("/findAllNewsType")
    @ResponseBody
    public Result findAllNewsType(@RequestParam(value = "newsTypeName",required = false) String newsTypeName){
        return iAcpNewsTypeService.findAllNewsType(newsTypeName);
    }


    /**
     * 分页查询
     * @return
     */
    @PostMapping("/findPageNewsType")
    @ResponseBody
    public Result findPageNewsType(@RequestParam(value = "newsTypeName",required = false) String newsTypeName ,PageDto pageDto){
        return iAcpNewsTypeService.findPageNewType(newsTypeName,pageDto);
    }





}
