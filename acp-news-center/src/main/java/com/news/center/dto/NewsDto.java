package com.news.center.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewsDto {
    /**
     * 新闻ID
     */
    private Long newsId;
    /**
     * 新闻标题
     */
    private String newsTitle;

    /**
     * 作者（后台管理发布者id）
     */
    private Long authorId;

    /**
     * 作者名称
     */
    private String authName;

    /**
     * 新闻来源
     */
    private String newsSource;

    /**
     * 新闻类型
     */
    private Long newsType;
    /**
     * 新闻浏览量
     */
    private Long newsViews;

    /**
     * 新闻内容(富文本)
     */
    private String newsContent;

    /**
     * 点赞数
     */
    private Long newsSupportNumber;

    /**
     * 收藏数
     */
    private Long newsCollectionsNumber;

    /**
     * 新闻状态（1发布，2保存，3下架）
     */
    private Long newsStatus;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 结束时间
     */
    private String endTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 逻辑删除(0，未删除，1，删除)
     */
    private Integer status;

    /**
     * 新闻类型名称
     */
    private String newsTypeName;

    /**
     * 评论数量
     */
    private int commentCount;

}
