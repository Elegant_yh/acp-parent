package com.news.center.common;

/**
 * 公共的返回码
 *    返回码code：
 *      成功：200
 *      失败：500
 *      未登录：401
 *      未授权：403
 *      抛出异常：503
 */
public enum ResultCode {

    SUCCESS(true,200,"操作成功！"),
    //---系统错误返回码-----
    FAIL(false,500,"操作失败"),

    UNAUTHENTICATED(false,401,"您还未登录"),
    UNAUTHORISE(false,403,"权限不足"),
    SERVER_ERROR(false,503,"抱歉，系统繁忙，请稍后重试！"),

    //---用户操作返回码  2xxxx----
    MOBILEORPASSWORDERROR(false,20001,"用户名或密码错误"),

    //---企业操作返回码  3xxxx----
    //---权限操作返回码----
    //---其他操作返回码----
    NOT_FOUNT(false,40006,"没有查询到相关数据");
    //操作是否成功
    boolean success;
    //操作代码
    int code;
    //提示信息
    String message;

    ResultCode(boolean success,int code, String message){
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public boolean success() {
        return success;
    }

    public int code() {
        return code;
    }

    public String message() {
        return message;
    }

}
