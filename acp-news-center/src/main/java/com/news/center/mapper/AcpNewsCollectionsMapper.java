package com.news.center.mapper;

import com.news.center.entity.AcpNewsCollections;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
public interface AcpNewsCollectionsMapper extends BaseMapper<AcpNewsCollections> {

}
