package com.news.center.mapper;

import com.news.center.entity.AcpNewsType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
public interface AcpNewsTypeMapper extends BaseMapper<AcpNewsType> {

    // 添加
    int saveNewsType(AcpNewsType acpNewsType);

    // 修改
    int modifyNewsType(AcpNewsType acpNewsType);
}
