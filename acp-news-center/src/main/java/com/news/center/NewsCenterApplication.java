package com.news.center;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.news.center.mapper")
@EnableFeignClients(basePackages = "com.news.center.feign")
public class NewsCenterApplication {
    public static void main(String[] args){
        SpringApplication.run(NewsCenterApplication.class,args);
    }
}
