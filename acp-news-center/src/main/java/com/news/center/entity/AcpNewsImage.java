package com.news.center.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AcpNewsImage extends Model<AcpNewsImage> {

    private static final long serialVersionUID = 1L;

    /**
     * 新闻图片id
     */
    @TableId(value = "news_images_id", type = IdType.AUTO)
    private Long newsImagesId;

    /**
     * 新闻图片路径
     */
    private String newsImagesUrl;

    /**
     * 新闻id （该新闻图片对应的详情新闻）
     */
    private Long newsId;

    /**
     * 预留字段1
     */
    private String text1;

    /**
     * 预留字段2
     */
    private String text2;

    /**
     * 预留字段3
     */
    private String text3;


    @Override
    protected Serializable pkVal() {
        return this.newsImagesId;
    }

}
