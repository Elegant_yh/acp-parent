package com.news.center.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AcpNewsType extends Model<AcpNewsType> {

    private static final long serialVersionUID = 1L;

    /**
     * 新闻类型地id
     */
    @TableId(value = "news_type_id", type = IdType.AUTO)
    private Long newsTypeId;

    /**
     * 新闻类型名称
     */
    private String newsTypeName;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 逻辑删除(0，未删除，1，删除)
     */
    @TableLogic(value = "0",delval = "1")
    private Integer status;

    /**
     * 预留字段1
     */
    private String text1;

    /**
     * 预留字段2
     */
    private String text2;

    /**
     * 预留字段3
     */
    private String text3;


    @Override
    protected Serializable pkVal() {
        return this.newsTypeId;
    }

}
