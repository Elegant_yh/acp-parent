package com.news.center.service.impl;

import com.news.center.entity.AcpNewsCollections;
import com.news.center.mapper.AcpNewsCollectionsMapper;
import com.news.center.service.IAcpNewsCollectionsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Service
public class AcpNewsCollectionsServiceImpl extends ServiceImpl<AcpNewsCollectionsMapper, AcpNewsCollections> implements IAcpNewsCollectionsService {

}
