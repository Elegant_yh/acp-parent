package com.news.center.service.impl;

import com.news.center.entity.AcpNewsCommentSupport;
import com.news.center.mapper.AcpNewsCommentSupportMapper;
import com.news.center.service.IAcpNewsCommentSupportService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Service
public class AcpNewsCommentSupportServiceImpl extends ServiceImpl<AcpNewsCommentSupportMapper, AcpNewsCommentSupport> implements IAcpNewsCommentSupportService {

}
