package com.news.center.service;

import com.news.center.common.Result;
import com.news.center.dto.NewsDto;
import com.news.center.dto.PageDto;
import com.news.center.entity.AcpNewsDetails;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
public interface IAcpNewsDetailsService extends IService<AcpNewsDetails> {
    // 保存
    Result saveNews(AcpNewsDetails acpNewsDetails);

    // 发布
    Result releaseNews(AcpNewsDetails acpNewsDetails);

    // 分页查询
    Result findAllPage(PageDto pageDto , NewsDto newsDto);

    // 根据新闻ID 修改新闻状态
    Result newsStatus(Long newsStatus, Long newsId);

    // 删除
    Result deleteNews(Long newsId);
}
