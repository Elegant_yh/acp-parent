package com.news.center.service;

import com.news.center.entity.AcpNewsSupport;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
public interface IAcpNewsSupportService extends IService<AcpNewsSupport> {

}
