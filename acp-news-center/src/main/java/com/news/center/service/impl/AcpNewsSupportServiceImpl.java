package com.news.center.service.impl;

import com.news.center.entity.AcpNewsSupport;
import com.news.center.mapper.AcpNewsSupportMapper;
import com.news.center.service.IAcpNewsSupportService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Service
public class AcpNewsSupportServiceImpl extends ServiceImpl<AcpNewsSupportMapper, AcpNewsSupport> implements IAcpNewsSupportService {

}
