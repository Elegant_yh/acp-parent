package com.zx.coupon.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zx.coupon.entity.CouponEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 
 * 
 * @author xz
 * @email sunlightcs@gmail.com
 * @date 2020-09-17 16:20:16
 */
@Mapper
@Repository
public interface CouponDao {

    IPage<CouponEntity> getPage(Page<CouponEntity> page, QueryWrapper queryWrapper);

    Integer save(CouponEntity coupon);

    Integer update(CouponEntity coupon);

    Integer del(Integer couponId);

    CouponEntity getById(Integer couponId);
}
