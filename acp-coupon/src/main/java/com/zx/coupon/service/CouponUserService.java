package com.zx.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.coupon.common.utils.PageUtils;
import com.zx.coupon.entity.CouponUserEntity;

import java.util.Map;

/**
 * 
 *
 * @author xz
 * @email sunlightcs@gmail.com
 * @date 2020-09-17 16:20:16
 */
public interface CouponUserService extends IService<CouponUserEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

