package com.zx.coupon.service.impl;

import com.zx.coupon.dao.CouponTypeDao;
import com.zx.coupon.entity.CouponTypeEntity;
import com.zx.coupon.service.CouponTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CouponTypeServiceImpl implements CouponTypeService {

    @Autowired
    private CouponTypeDao couponTypeDao;

    @Override
    public List<CouponTypeEntity> findAll() {
        List<CouponTypeEntity> couponTypeEntity = couponTypeDao.findAll();
        return couponTypeEntity;
    }
}
