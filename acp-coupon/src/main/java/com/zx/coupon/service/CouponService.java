package com.zx.coupon.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zx.coupon.common.utils.PageUtils;
import com.zx.coupon.entity.CouponEntity;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author xz
 * @email sunlightcs@gmail.com
 * @date 2020-09-17 16:20:16
 */
public interface CouponService {


    IPage<CouponEntity> getPage(Page<CouponEntity> page, QueryWrapper queryWrapper);

    CouponEntity getById(Integer couponId);

    Integer save(CouponEntity coupon);

    Integer updateById(CouponEntity coupon);

    Integer removeByIds(Integer couponId);
}

