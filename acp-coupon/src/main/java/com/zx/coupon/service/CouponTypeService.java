package com.zx.coupon.service;

import com.zx.coupon.common.utils.CommonResult;
import com.zx.coupon.entity.CouponTypeEntity;

import java.util.List;

public interface CouponTypeService {
    List<CouponTypeEntity> findAll();
}
