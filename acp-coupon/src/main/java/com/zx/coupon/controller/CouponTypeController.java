package com.zx.coupon.controller;


import com.zx.coupon.common.utils.CommonResult;
import com.zx.coupon.entity.CouponTypeEntity;
import com.zx.coupon.service.CouponTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/couponType")
public class CouponTypeController {

    @Autowired
    private CouponTypeService couponTypeService;

    /**
     * 查询所有类型
     */
    @GetMapping("/findAll")
    public CommonResult find() {
        List<CouponTypeEntity> list = couponTypeService.findAll();
        HashMap map = new HashMap<>();
        if (list.size() != 0 && list != null) {
            map.put("msg", "查询成功");
            map.put("data", list);
            map.put("code", 0);
        } else {
            map.put("msg", "查询失败");
            map.put("code", 1);
        }
        return CommonResult.ok(map);
    }
}
