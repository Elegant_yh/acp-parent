package com.zx.coupon.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author xz
 * @email sunlightcs@gmail.com
 * @date 2020-09-17 16:20:16
 */
@Data
@TableName("acp_coupon_user")
public class CouponUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 优惠券id
	 */
	private Long couponId;
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 用户名
	 */
	private String userUsername;
	/**
	 * 获取方式【0->后台赠送；1->主动领取】
	 */
	private Integer getType;
	/**
	 * 创建时间
	 */
	private Date creatTime;
	/**
	 * 使用状态【0->未使用；1->已使用；2->已过期】
	 */
	private Integer useType;
	/**
	 * 使用时间
	 */
	private Date useTime;
	/**
	 * 预留字段1
	 */
	private String text1;
	/**
	 * 预留字段2
	 */
	private String text2;
	/**
	 * 预留字段3
	 */
	private String text3;

}
