package com.zx.coupon.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("acp_coupon_type")
public class CouponTypeEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 优惠券类型id
     */
    @TableId
    private Integer couponTypeId;

    /**
     * 优惠券类型名称
     */
    private String couponTypeName;
    /**
     * 新增时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 预留字段1
     */
    private String text1;
    /**
     * 预留字段2
     */
    private String text2;
    /**
     * 预留字段3
     */
    private String text3;
}
