package com.zx.ad.dao;

import com.zx.ad.entity.ComboPriceEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 
 * 
 * @author lilei
 * @email mrstupide@163.com
 * @date 2020-09-17 14:27:02
 */
@Mapper
public interface ComboPriceDao {

    List<ComboPriceEntity> findAll();
}
