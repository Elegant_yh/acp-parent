package com.zx.ad.service.impl;

import com.zx.ad.dao.ComboPriceDao;
import com.zx.ad.entity.ComboEntity;
import com.zx.ad.entity.ComboPriceEntity;
import com.zx.ad.service.ComboPriceService;
import com.zx.ad.utils.common.PageResult;
import com.zx.ad.utils.common.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static feign.form.FormData.builder;

@Service("comboPriceService")
public class ComboPriceServiceImpl implements ComboPriceService {

    @Autowired
    private ComboPriceDao comboPriceDao;

    @Override
    public void removeByIds(List<Long> asList) {

    }

    @Override
    public void updateById(ComboPriceEntity comboPrice) {

    }

    @Override
    public void save(ComboPriceEntity comboPrice) {

    }

    @Override
    public ComboPriceEntity getById(Long priceId) {
        return null;
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        return null;
    }

    @Override
    public PageResult<ComboPriceEntity> findAll() {
        List<ComboPriceEntity> list = comboPriceDao.findAll();
        return PageResult.<ComboPriceEntity>builder().data(list).code(0).build();
    }
}
