package com.zx.ad.controller;

import java.util.Arrays;

import com.zx.ad.entity.ComboEntity;
import com.zx.ad.service.ComboService;
import com.zx.ad.utils.common.CommonResult;
import com.zx.ad.utils.common.PageResult;
import com.zx.ad.vo.ComboSearchEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 *
 *
 * @author lilei
 * @email mrstupide@163.com
 * @date 2020-09-17 14:27:02
 */
@RestController
@Api(tags = "套餐模块api")
@Slf4j
@RequestMapping("/ad/combo")
//@RestController
public class ComboController {

    @Autowired
    private ComboService comboService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @ApiOperation(value = "查询所有套餐")
    public PageResult<ComboEntity> list(ComboSearchEntity comboSearchEntity) {
        return comboService.queryPage(comboSearchEntity);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{comboId}")
    @ApiOperation(value = "根据id查询套餐")
    // @RequiresPermissions("ad:combo:info")
    public CommonResult info(@PathVariable("comboId") Long comboId){
        ComboEntity combo = comboService.getById(comboId);
        return CommonResult.ok().put("combo", combo);
    }

    /**
     * 保存
     */
    @RequestMapping("/saveOrUpdate")
    @ApiOperation(value = "保存套餐")
    // @RequiresPermissions("ad:combo:save")
    public CommonResult save(@RequestBody ComboEntity combo){
        try {
            if (StringUtils.isEmpty(combo.getComboId())) {
                comboService.save(combo);
            } else {
                comboService.updateById(combo);
            }
            return CommonResult.ok("操作成功");
        } catch (Exception e) {
            log.error("ad-saveOrUpdate-error", e);
            return CommonResult.error("操作失败");
        }
    }

    /**
     * 删除
     */
    @ApiOperation(value = "删除套餐")
    @DeleteMapping("/delete/{ids}")
    // @RequiresPermissions("ad:combo:delete")
    public CommonResult delete(@PathVariable Long[] ids){
        try {
            comboService.removeByIds(Arrays.asList(ids));
            return CommonResult.ok("操作成功");
        } catch (Exception e) {
            log.error("ad-delete-error", e);
            return CommonResult.error("操作失败");
        }
    }

}
