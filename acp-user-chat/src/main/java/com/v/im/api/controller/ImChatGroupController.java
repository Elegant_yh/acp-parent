package com.v.im.api.controller;

import com.v.im.common.utils.CommonResult;
import com.v.im.user.entity.ImChatGroup;
import com.v.im.user.entity.ImUser;
import com.v.im.user.service.IImChatGroupService;
import com.v.im.user.service.IImUserFriendService;
import com.v.im.user.service.IImUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/api/ChatGroup")
@RefreshScope
public class ImChatGroupController {

    private final Logger logger = LoggerFactory.getLogger(ImUserController.class);
    @Autowired
    @Qualifier("imChatGroupServiceImpl")
    private IImChatGroupService iImChatGroupService;
    @Resource
    @Qualifier(value = "imUserService")
    private IImUserService imUserService;
    @Resource
    @Qualifier(value = "imUserFriendService")
    private IImUserFriendService imUserFriendService;

    //搜索群聊
    @PostMapping("/searchChatGroup")
    public CommonResult searchChatGroup(@RequestParam String id) {
        return CommonResult.success(iImChatGroupService.searchChatGroup(id));
    }

    //创建群聊
    @PutMapping("/addChatGroup")
    public CommonResult addChatGroup(ImChatGroup imChatGroup) {
        //获取好友信息
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ImUser user = imUserService.getByLoginName(username);
        imChatGroup.setMaster(user.getLoginName());
        //imUserFriendService.getUserFriends(user.getId());
       //添加到群聊
        if(iImChatGroupService.addChatGroup(imChatGroup)<1){
            return CommonResult.failed("添加失败");
        }
        return CommonResult.success("添加成功");
    }

    //删除群聊
    @DeleteMapping("/delChatGroup")
    public CommonResult delChatGroup(@RequestParam String id) {
        return iImChatGroupService.delChatGroup(id);
    }

    //退出群聊
    @PostMapping("/exitGroupChat")
    public CommonResult exitGroupChat(@RequestParam String chatId,boolean isHost) {
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ImUser user = imUserService.getByLoginName(username);
        return CommonResult.success(iImChatGroupService.exitGroupChat(chatId,isHost,user.getId()));
    }

    //修改群信息
    @PutMapping("/updateChatGroup")
    public CommonResult updateChatGroup(@RequestBody ImChatGroup imChatGroup) {
        iImChatGroupService.updateChatGroup(imChatGroup);
        return CommonResult.success();
    }

    @PostMapping("/enterChatGroup")
    public CommonResult enterChatGroup(String chatGroupId){
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ImUser user = imUserService.getByLoginName(username);
        int i=iImChatGroupService.enterChatGroup(chatGroupId,user.getId());
        if(i>0){
            return CommonResult.success(i,"加入成功");
        }else{
            return CommonResult.failed("加入失败");
        }

    }
}
