package com.v.im.api.controller;

import com.v.im.common.utils.CommonResult;
import com.v.im.user.entity.ImUser;
import com.v.im.user.entity.ImUserFriend;
import com.v.im.user.service.IImUserFriendService;
import com.v.im.user.service.IImUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/friend")
@RefreshScope
public class ImFriendController {
    private final Logger logger = LoggerFactory.getLogger(ImUserController.class);
    @Autowired
    @Qualifier(value = "imUserFriendService")
    private IImUserFriendService iImUserFriendService;

    @Resource
    @Qualifier(value = "imUserService")
    private IImUserService imUserService;


    /**
     * 搜索好友
     */
    @PostMapping("/SearchFriends")
    public List<ImUser> SearchFriends(@RequestParam String mobile) {
        return imUserService.SearchUsers(mobile);
    }


    /**
     * 添加好友
     */
    @PostMapping("/addFriends")
    public CommonResult addFriends(ImUserFriend imUserFriend) {

        CommonResult list = iImUserFriendService.addFriends(imUserFriend);
        return list;
    }

    /**
     * 删除好友
     */
    @DeleteMapping("/delFriends")
    public CommonResult delFriends(@RequestParam String userId) {
        return iImUserFriendService.delFriends(userId);
    }



}
