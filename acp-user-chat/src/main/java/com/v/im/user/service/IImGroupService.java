package com.v.im.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.v.im.common.utils.CommonResult;
import com.v.im.user.entity.ImGroup;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 乐天
 * @since 2018-10-23
 */
public interface IImGroupService extends IService<ImGroup> {
    //新增好友分组
    int addGroup(ImGroup imGroup);

    //删除好友分组
    int delGroup(String s, String groupId, String loginName);

    CommonResult getGroup(String id);


//   int addGroup(ImGroup imGroup, String id);

    int updateGroup(String id, String name, String userId);

    //修改friend_group_id
    CommonResult updateFriendGroupId(String userId);
}
