package com.v.im.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.v.im.common.utils.CommonResult;
import com.v.im.user.entity.ImChatGroup;

import java.util.List;

/**
 * <p>
 * 群 服务类
 * </p>
 *
 * @author 乐天
 * @since 2018-10-28
 */
public interface IImChatGroupService extends IService<ImChatGroup> {
    /**
     * 创建群聊
     * @param imChatGroup
     * @return
     */
    int addChatGroup(ImChatGroup imChatGroup);

    /**
     * 删除群聊
     * @param id
     * @return
     */
    CommonResult delChatGroup(String id);

    /**
     * 修改群信息
     * @param imChatGroup
     * @return
     */
    CommonResult updateChatGroup(ImChatGroup imChatGroup);

    /**
     * 查询群组信息
     * @param id
     * @return
     */
    List<ImChatGroup> searchChatGroup(String id);

    /**
     * 退出群聊
     * @param chatId
     * @param isHost
     * @param userId
     * @return
     */
    boolean exitGroupChat(String chatId, boolean isHost, String userId);

    /**
     * 加入群聊
     * @param chatGroupId
     * @param userId
     * @return
     */
    int enterChatGroup(String chatGroupId, String userId);
}
