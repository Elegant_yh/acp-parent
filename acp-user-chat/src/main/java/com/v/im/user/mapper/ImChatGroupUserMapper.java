package com.v.im.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.v.im.user.entity.ImChatGroupUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

/**
 * <p>
 * 群 Mapper 接口
 * </p>
 *
 * @author 乐天
 * @since 2018-10-28
 */
public interface ImChatGroupUserMapper extends BaseMapper<ImChatGroupUser> {

    Integer selectByChatGroupId(String chatGroupId);

    Integer deleteByUserId(@Param("userId") String userId, @Param("chatId") String chatId);
}
