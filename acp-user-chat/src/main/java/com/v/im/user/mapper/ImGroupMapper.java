package com.v.im.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.v.im.common.utils.CommonResult;
import com.v.im.user.entity.ImGroup;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 乐天
 * @since 2018-10-23
 */
public interface ImGroupMapper extends BaseMapper<ImGroup> {

    CommonResult getGroup(String id);

    //删除分组
    int delGroup(@Param("id") String id, @Param("userName") String userId);

    int updateGroup(@Param("id") String id, @Param("name") String name, @Param("userId") String userId);

    //修改FriendGroupId
    CommonResult updateFriendGroupId(String userId);

    List<ImGroup> selectByName(ImGroup imGroup);

    List<ImGroup> selectByUserId(String id);

    int breakGroup(@Param("groupId") String groupId, @Param("id") String id);
}
