package com.zx.acpactivity.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


/**
 * <p>
 *
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Accessors(chain = true)
public class AcpActivityTemplateFavorites extends Model<AcpActivityTemplateFavorites> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 活动模板id
     */
    private Long activityTemplateId;


}
