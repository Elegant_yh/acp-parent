package com.zx.acpactivity.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AcpActivityStatus {
    private String status;
    private Integer count;
}
