package com.zx.acpactivity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Accessors(chain = true)
public class AcpActivityParticipant extends Model<AcpActivityParticipant> {

    private static final long serialVersionUID=1L;

    public AcpActivityParticipant(Long activityId, Long participantId) {
        this.activityId = activityId;
        this.participantId = participantId;
    }

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 活动id
     */
    private Long activityId;


    /**
     * 参与者id
     */
    private Long participantId;
}
