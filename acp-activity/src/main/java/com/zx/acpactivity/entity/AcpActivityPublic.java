package com.zx.acpactivity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public abstract class AcpActivityPublic<T extends Model<?>> extends Model<T> {

    /**
     * 活动id
     */
    @TableId(value = "activity_id", type = IdType.AUTO)
    private Long activityId;

    /**
     * 活动类型id
     */
    private Long activityTypeId;

    /**
     * 活动类型名称
     */
    private String activityTypeName;

    /**
     * 节点唯一标志 活动节点类型名+活动节点id!!!!!!!!! 2020-10-14 修改为邀请码，格式为UUID前5位+活动id
     */
    private String code;

    /**
     * 活动对外公开状态，0=>私有，1=>公开 ,2=>半公开
     */
    private Integer showStatus;

    /**
     * 审核人id
     */
    private Long staffId;

    /**
     * 审核人名字
     */
    private String staffName;

    /**
     * 活动名称
     */
    private String name;

    /**
     * 活动主题，简述
     */
    private String topic;

    /**
     * 活动描述
     */
    private String description;

    /**
     * 活动海报
     */
    private String poster;

    /**
     * 活动广告
     */
    private String ad;

    /**
     * 发布人的id
     */
    private Long userId;

    /**
     * 发布人姓名
     */
    private String userName;

    /**
     * 发布人电话
     */
    private String userPhone;

    /**
     * 发布人类型，0=>个人，1=>企业
     */
    private Integer userType;

    /**
     * 活动开始时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    /**
     * 活动结束时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    /**
     * 允许参与人数
     */
    private Integer size;

    /**
     * 报名人数
     */
    private Integer signUpNum;

    /**
     * 活动地点
     */
    private String location;

    /**
     * 活动状态：0=>保存未提交审核，1=>提交待审核，2=>审核通过，发布状态，活动未开始，3=>审核未通过，4=>活动进行中，5=>活动以结束，6=>活动异常终止
     */
    private Integer activityStatus;

    /**
     * 状态信息，包括审核不通过理由，活动异常终止原因
     */
    private String statusInfo;

    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 逻辑删除的状态，0=>未删除，1=>删除
     */
    private Integer status;

    /**
     * 预留字段1
     */
    private String text1;

    /**
     * 预留字段2
     */
    private String text2;

    /**
     * 预留字段3
     */
    private String text3;

    /**
     * 预留字段4
     */
    private Integer int_1;

    /**
     * 预留字段5
     */
    private Integer int_2;

}
