package com.zx.acpactivity.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Accessors(chain = true)
public class FormSelectTree {
    private Long id;
    private String name;
    private Boolean open;
    private Boolean checked;
    private List<FormSelectTree> children;
}
