package com.zx.acpactivity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Accessors(chain = true)
public class AcpActivityMemberInfo extends Model<AcpActivityMemberInfo> {

    private static final long serialVersionUID=1L;

    /**
     * 成员信息id
     */
    @TableId(value = "member_info_id", type = IdType.AUTO)
    private Long memberInfoId;

    /**
     * 活动id
     */
    private Long activityId;

    /**
     * 成员名字或昵称
     */
    private String name;

    /**
     * 成员联系方式
     */
    private String phone;

    /**
     * 成员其他信息
     */
    private String otherInfo;

    /**
     * 申请参与人数，默认1
     */
    private Integer memberCount;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除的状态，0=>未删除，1=>删除
     */
    private Integer status;

    /**
     * 预留字段1
     */
    private String text1;

    /**
     * 预留字段2
     */
    private String text2;

    /**
     * 预留字段3
     */
    private String text3;



}
