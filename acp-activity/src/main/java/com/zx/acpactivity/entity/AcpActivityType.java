package com.zx.acpactivity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Accessors(chain = true)
public class AcpActivityType extends Model<AcpActivityType> {

    private static final long serialVersionUID=1L;

    /**
     * 活动类型id
     */
    @TableId(value = "activity_type_id", type = IdType.AUTO)
    private Long activityTypeId;

    /**
     * 活动类型父类id，如果没有父类，默认为零
     */
    private Long parentId;

    /**
     * 活动类型名称
     */
    private String name;

    /**
     * id分类路径 格式：parent_id，parent_id，...,activity_type_id
     */
    private String idPath;

    /**
     * 活动类型层级，1-3
     */
    private Integer level;

    /**
     * 是否显示[0-不显示，1显示]
     */
    private Integer showStatus;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除的状态，0=>未删除，1=>删除
     */
    private Integer status;

    /**
     * 预留字段1
     */
    private String text1;

    /**
     * 预留字段2
     */
    private String text2;

    /**
     * 预留字段3
     */
    private String text3;

}
