package com.zx.acpactivity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public abstract class AcpActivityNodePublic<T extends Model<?>> extends Model<T> {

    /**
     * 活动节点id
     */
    @TableId(value = "node_id", type = IdType.AUTO)
    private Long nodeId;

    /**
     * 活动id
     */
    private Long activityId;

    /**
     * 活动节点名称
     */
    private String name;

    /**
     * 活动节点类型id
     */
    private Long nodeTypeId;

    /**
     * 活动节点类型名
     */
    private String nodeTypeName;

    /**
     * 节点唯一标志 活动节点类型名+活动节点id
     */
    private String code;

    /**
     * 活动节点负责人
     */
    private String principalName;

    /**
     * 活动节点负责人电话
     */
    private String principalPhone;

    /**
     * 活动节点内容
     */
    private String content;

    /**
     * 活动节点开始时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    /**
     * 活动节点结束时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    /**
     * 活动节点状态：0=>活动节点未开始，1=>活动进行中，2=>活动以结束，3=>活动异常终止
     */
    private Integer nodeStatus;

    /**
     * 活动节点地点
     */
    private String site;

    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 逻辑删除的状态，0=>未删除，1=>删除
     */
    private Integer status;

    /**
     * 预留字段1
     */
    private String text1;

    /**
     * 预留字段2
     */
    private String text2;

    /**
     * 预留字段3
     */
    private String text3;

}
