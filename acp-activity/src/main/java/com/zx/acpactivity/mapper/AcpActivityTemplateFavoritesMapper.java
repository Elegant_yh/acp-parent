package com.zx.acpactivity.mapper;

import com.zx.acpactivity.entity.AcpActivityTemplateFavorites;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
public interface AcpActivityTemplateFavoritesMapper extends BaseMapper<AcpActivityTemplateFavorites> {

}
