package com.zx.acpactivity.mapper;

import com.zx.acpactivity.entity.AcpActivityType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
public interface AcpActivityTypeMapper extends BaseMapper<AcpActivityType> {
    List<AcpActivityType> findByLevel();
}
