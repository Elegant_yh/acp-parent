package com.zx.acpactivity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zx.acpactivity.entity.AcpActivityParticipant;
import org.springframework.stereotype.Repository;

@Repository
public interface AcpActivityParticipantMapper extends BaseMapper<AcpActivityParticipant> {

}
