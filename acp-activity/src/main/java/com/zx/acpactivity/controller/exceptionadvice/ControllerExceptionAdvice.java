package com.zx.acpactivity.controller.exceptionadvice;

import com.zx.acpactivity.utils.Result;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
class ControllerExceptionAdvice {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result<Object> handlerException(Exception e) {
        e.printStackTrace();
        return Result.failed("操作失败");
    }

    @ExceptionHandler(BindException.class)
    @ResponseBody
    public Result<Object> handleMethodArgumentNotValidException(BindException e) {
        try {
            return Result.validateFailed(e.getBindingResult().getFieldError().getDefaultMessage());
        } catch (Exception ex) {
            return Result.failed("未知错误");
        }
    }
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public Result<Object> handlerMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        try {
            return Result.validateFailed(e.getBindingResult().getFieldError().getDefaultMessage());
        } catch (Exception ex) {
            return Result.failed("未知错误");
        }
    }

}
