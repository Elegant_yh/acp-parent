package com.zx.acpactivity.feign.impl;

import com.zx.acpactivity.feign.UserFeignService;
import com.zx.acpactivity.utils.Result;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * com.zx.acpactivity.feign.impl
 * name：UserFeignServiceImpl
 * user：pengpeng
 * date:2020/10/22
 * time:10:06
 */
@Component
public class UserFeignServiceImpl implements UserFeignService {
    @Override
    public Result getUserById(Long id) {
        return Result.failed("服务器出现问题了~，请稍后再试哦");
    }
}
