package com.zx.acpactivity.feign.impl;

import com.zx.acpactivity.feign.AliMessageFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * com.zx.acpactivity.service.impl
 * name：AliMessageFeignServiceImpl
 * user：pengpeng
 * date:2020/10/15
 * time:17:54
 */
@Component
@Slf4j
public class AliMessageFeignServiceImpl implements AliMessageFeignService {
    @Override
    public void sendMsg(String mobile, String code) {
       log.error("用户 {} 验证码 {} 发送失败",mobile,code);
    }
}
