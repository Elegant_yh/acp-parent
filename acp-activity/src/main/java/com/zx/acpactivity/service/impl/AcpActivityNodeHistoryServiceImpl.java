package com.zx.acpactivity.service.impl;

import com.zx.acpactivity.entity.AcpActivityNodeHistory;
import com.zx.acpactivity.mapper.AcpActivityNodeHistoryMapper;
import com.zx.acpactivity.service.AcpActivityNodeHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-22
 */
@Service
public class AcpActivityNodeHistoryServiceImpl extends ServiceImpl<AcpActivityNodeHistoryMapper, AcpActivityNodeHistory> implements AcpActivityNodeHistoryService {
    @Autowired
    AcpActivityNodeHistoryMapper acpActivityNodeHistoryMapper;
    @Override
    public boolean insert(AcpActivityNodeHistory nodeHistory) {
        return 0!=acpActivityNodeHistoryMapper.insert(nodeHistory);
    }
}
