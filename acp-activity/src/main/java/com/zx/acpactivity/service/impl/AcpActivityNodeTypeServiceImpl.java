package com.zx.acpactivity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.acpactivity.entity.AcpActivityNodeTemplate;
import com.zx.acpactivity.entity.AcpActivityNodeType;
import com.zx.acpactivity.entity.FormSelectTree;
import com.zx.acpactivity.mapper.AcpActivityNodeTemplateMapper;
import com.zx.acpactivity.mapper.AcpActivityNodeTypeMapper;
import com.zx.acpactivity.service.AcpActivityNodeTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-22
 */
@Service
public class AcpActivityNodeTypeServiceImpl extends ServiceImpl<AcpActivityNodeTypeMapper, AcpActivityNodeType> implements AcpActivityNodeTypeService {

    @Resource
    private AcpActivityNodeTypeMapper acpActivityNodeTypeMapper;

    @Autowired
    private AcpActivityNodeTemplateMapper acpActivityNodeTemplateMapper;

    @Override
    @Transactional
    public boolean mySaveOrUpDate(AcpActivityNodeType acpActivityNodeType) {
        boolean flag;
        Long activityTypeId = acpActivityNodeType.getNodeTypeId();
        Long parentId = acpActivityNodeType.getParentId();
        String parentIdPath = "";
        if (activityTypeId == null) {
            if (parentId == null) {
                acpActivityNodeType.setLevel(1);
            } else {
                AcpActivityNodeType nodeType = acpActivityNodeTypeMapper.selectById(parentId);
                acpActivityNodeType.setLevel(nodeType.getLevel() + 1);
                parentIdPath = nodeType.getIdPath();
            }
            LocalDateTime now = LocalDateTime.now();
            acpActivityNodeType.setCreateTime(now);
            acpActivityNodeType.setUpdateTime(now);
            flag = acpActivityNodeTypeMapper.insert(acpActivityNodeType) > 0;
            Long nodeTypeId = acpActivityNodeType.getNodeTypeId();
            acpActivityNodeTypeMapper.updateById(new AcpActivityNodeType().setNodeTypeId(nodeTypeId)
                    .setIdPath((parentIdPath.equals("") ? parentIdPath : (parentIdPath + ",")) + nodeTypeId));
        } else {
            flag = acpActivityNodeTypeMapper.updateById(acpActivityNodeType) > 0;
            try {
                AcpActivityNodeTemplate acpActivityNodeTemplate = new AcpActivityNodeTemplate()
                        .setName(acpActivityNodeType.getName());
                UpdateWrapper<AcpActivityNodeTemplate> wrapper = new UpdateWrapper<>();
                wrapper.lambda().eq(AcpActivityNodeTemplate::getNodeTypeId, acpActivityNodeType.getNodeTypeId());
                acpActivityNodeTemplateMapper.update(acpActivityNodeTemplate, wrapper);
            } catch (Exception e) {
            }
        }
        return flag;
    }

    @Override
    @Transactional
    public boolean deleteById(Long id) {
        boolean flag = false;
        QueryWrapper<AcpActivityNodeType> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id", id);
        List<AcpActivityNodeType> acpActivityTypeList = acpActivityNodeTypeMapper.selectList(queryWrapper);
        if (acpActivityTypeList == null || acpActivityTypeList.size() == 0)
            flag = acpActivityNodeTypeMapper.deleteById(id) > 0;
        return flag;
    }

    @Override
    public List<FormSelectTree> findTypeTree() {
        HashMap<Long, FormSelectTree> fstMap = new HashMap<>();
        List<FormSelectTree> formSelectTrees = new ArrayList<>();
        list().stream()
                .peek(ant ->
                        fstMap.put(ant.getNodeTypeId(), new FormSelectTree()
                                .setId(ant.getNodeTypeId())
                                .setName(ant.getName())
                                .setOpen(true))
                )
                .forEach(ant -> {
                    if (fstMap.containsKey(ant.getParentId())) {
                        FormSelectTree formSelectTree = fstMap.get(ant.getParentId());
                        List<FormSelectTree> children = formSelectTree.getChildren();
                        if (children == null) {
                            children = new ArrayList<>();
                            formSelectTree.setChildren(children);
                        }
                        children.add(fstMap.get(ant.getNodeTypeId()));
                    } else
                        formSelectTrees.add(fstMap.get(ant.getNodeTypeId()));
                });
        return formSelectTrees;
    }
}
