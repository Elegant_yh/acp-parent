package com.zx.acpactivity.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.acpactivity.entity.AcpActivityParticipant;
import com.zx.acpactivity.mapper.AcpActivityParticipantMapper;
import com.zx.acpactivity.service.AcpActivityParticipantService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class AcpActivityParticipantServiceImpl extends ServiceImpl<AcpActivityParticipantMapper, AcpActivityParticipant> implements AcpActivityParticipantService {
    @Resource
    AcpActivityParticipantMapper acpActivityParticipantMapper;

    @Override
    public boolean insert(AcpActivityParticipant acpActivityParticipant) {
        return 0!=acpActivityParticipantMapper.insert(acpActivityParticipant);
    }
}
