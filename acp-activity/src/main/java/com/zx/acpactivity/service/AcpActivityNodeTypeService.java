package com.zx.acpactivity.service;

import com.zx.acpactivity.entity.AcpActivityNodeType;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.acpactivity.entity.FormSelectTree;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-22
 */
public interface AcpActivityNodeTypeService extends IService<AcpActivityNodeType> {

    boolean mySaveOrUpDate(AcpActivityNodeType acpActivityNodeType);

    boolean deleteById(Long id);

    List<FormSelectTree> findTypeTree();
}
