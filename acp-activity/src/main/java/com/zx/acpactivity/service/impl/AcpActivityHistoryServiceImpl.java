package com.zx.acpactivity.service.impl;

import com.zx.acpactivity.entity.AcpActivityHistory;
import com.zx.acpactivity.mapper.AcpActivityHistoryMapper;
import com.zx.acpactivity.service.AcpActivityHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-22
 */
@Service
public class AcpActivityHistoryServiceImpl extends ServiceImpl<AcpActivityHistoryMapper, AcpActivityHistory> implements AcpActivityHistoryService {
    @Autowired
    AcpActivityHistoryMapper acpActivityHistoryMapper;
    @Override
    public boolean insert(AcpActivityHistory acpActivityHistory) {
        return 0!=acpActivityHistoryMapper.insert(acpActivityHistory);
    }
}
