package com.zx.acpactivity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zx.acpactivity.entity.AcpActivityNode;
import com.zx.acpactivity.mapper.AcpActivityNodeMapper;
import com.zx.acpactivity.service.AcpActivityNodeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
@Service
public class AcpActivityNodeServiceImpl extends ServiceImpl<AcpActivityNodeMapper, AcpActivityNode> implements AcpActivityNodeService {
    @Resource
    AcpActivityNodeMapper acpActivityNodeMapper;
    @Override
    public List<AcpActivityNode> getByActivityId(Long activityId) {
        QueryWrapper<AcpActivityNode> queryWrapper = new QueryWrapper();
        queryWrapper.eq("activity_id",activityId);
        return acpActivityNodeMapper.selectList(queryWrapper);
    }

    @Override
    public boolean deleteById(Long nodeId) {
        if (null==nodeId || nodeId<0)
            return false;
        return 0!=acpActivityNodeMapper.deleteById(nodeId);
    }

    @Override
    public boolean insert(AcpActivityNode node) {
        return 0!=acpActivityNodeMapper.insert(node);
    }
}
