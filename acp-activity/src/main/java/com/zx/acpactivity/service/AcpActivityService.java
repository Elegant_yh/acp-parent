package com.zx.acpactivity.service;

import com.zx.acpactivity.entity.AcpActivity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.acpactivity.entity.AcpActivityNode;
import com.zx.acpactivity.entity.AcpActivityStatus;
import com.zx.acpactivity.utils.PageResult;
import com.zx.acpactivity.utils.Result;
import com.zx.acpactivity.vo.AcpActivityVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
public interface AcpActivityService extends IService<AcpActivity> {
    /**
     * 发布活动
     *
     * @param acpActivity
     * @return
     */
    boolean creat(AcpActivity acpActivity, List<AcpActivityNode> acpActivityNodeList) throws Exception;

    boolean creat(AcpActivityVO acpActivityVo) throws Exception;

    /**
     * 审核活动
     * 上传数据：活动id(activityId)，审核理由(activityInfo)，审核人id(staffId)
     * @param acpActivity
     * @return
     */
    boolean checked(AcpActivity acpActivity);

    /**
     * 删除一个活动需要将其中活动表中删除=》在历史表中新增该活动=》删除其节点
     *
     * @param id
     * @return
     */
    boolean deleteById(Long id) throws Exception;

    Result updateByVo(AcpActivityVO acpActivityVo) throws Exception;

    /**
     * 条件查询活动
     *
     * @param acpActivityVO
     * @return
     */
    public PageResult<AcpActivity> findActivities(AcpActivityVO acpActivityVO);

    public Result<AcpActivity> selectActivityById(Integer id);

    public AcpActivity selectById(Long id);

    /**
     * 查询活动的所有节点
     */
    public Result<List<AcpActivityNode>> findNodesOfActivity(Long activityId);

    /**
     * 根据id查询活动
     */
    public Result selectActivityById(Long id);

    /**
     * 展示活动，不带节点,需要对活动进行条件查询，默认进行按时间排序
     * 条件：
     * 1. 公开，未公开
     * 2，是否在进行
     * 3，展示个数
     *
     * @param showStatus     1公开，0私有
     * @param activityStatus 0=>保存未提交审核，1=>提交待审核，2=>审核通过，发布状态，活动未开始，3=>审核未通过，4=>活动进行中，5=>活动以结束，6=>活动异常终止
     * @param limit          默认值20条
     * @return
     */
    Result<List<AcpActivity>> display(Integer showStatus, Integer activityStatus, Integer limit);

    /**
     * 根据活动id进行活动下架
     *
     * @param id
     * @return
     */
    Result revokeByIdAndCode(Long id, String code) throws Exception;

    boolean updateAllInfo(AcpActivityVO acpActivityVO);

    /**
     * 修改通过审核的的活动
     *
     * @param acpActivityVo
     * @return
     */
    public Result updateCheckedAC(AcpActivityVO acpActivityVo);

    /**
     * 正在活动中的活动，仅允许修改部分信息，例如活动结束时间
     *
     * @param acpActivityVo
     * @return
     */
    public Result updateUngoing(AcpActivityVO acpActivityVo);

    /**
     * 查询活动详情
     *
     * @param id
     * @return
     */
    Result<AcpActivityVO> getAcDetailById(Long id);

    /**
     * 获取下架验证码
     *
     * @param id
     * @return
     */
    Result getRevokeCodeById(Long id);

    /**
     * 查询用户发布的活动
     */
    List<AcpActivity> myPolishedActivities(Long userId);

    /**
     * 查询用户参与的活动
     */
    List<AcpActivity> myEngagedActivities(Long userId);

    Result joinAcpByIdAndUserId(Long userId, Long id,String code);

    String uploadNumberExcel(MultipartFile file);

    Result releaseWithExcel(AcpActivityVO acpActivityVO);

    List<AcpActivityStatus> countStatus();

    Result<List<AcpActivity>> findByIdPath(String idPath);
}
