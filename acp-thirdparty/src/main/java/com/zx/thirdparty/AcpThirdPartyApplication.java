package com.zx.thirdparty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 第三方服务公有
 */
@SpringBootApplication
@EnableDiscoveryClient
public class AcpThirdPartyApplication {

    public static void main(String[] args){
        SpringApplication.run(AcpThirdPartyApplication.class,args);
    }
}
