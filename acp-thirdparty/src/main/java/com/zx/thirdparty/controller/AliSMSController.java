package com.zx.thirdparty.controller;

import com.zx.thirdparty.common.alibaba.AliSMS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ali")
public class AliSMSController {

    @Autowired
    private AliSMS aliSMS;

    /**
     * 发送随机六位数验证码
     * @param mobile    电话号
     * @param code  验证码
     */
    @PostMapping("/sendSms")
    public void sendSms(@RequestParam("mobile") String mobile, @RequestParam("code") String code){
        aliSMS.sendSms(mobile,code);
    }
}
