package com.zx.user.center.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.user.center.entity.CompanyTotalDaily;
import com.zx.user.center.entity.CompanyTotalMonth;
import com.zx.user.center.entity.CompanyTotalYear;
import com.zx.user.center.entity.UserPersonalCenterEntity;
import com.zx.user.center.res.Result;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author
 * @email
 * @date 2020-09-16 18:40:42
 */
public interface UserPersonalCenterService extends IService<UserPersonalCenterEntity> {

    Result getById(Long id);
    List<UserPersonalCenterEntity> findAll();

    // 个人注册
    Result savePersonal(UserPersonalCenterEntity userPersonalCenterEntity);

    /**
     * 修改用户余额
     *
     * @param loginname
     * @param balance
     */
    void putBalance(String loginname, BigDecimal balance);

    //查询个人用户的数量
    Result QueryNumberOfPersonalUsers(Long userId);

    //查询个人用户日增长数
    Result QueryDailyGrowthP(Long userId);

    //查询个人用户月增长数
    Result QueryMonthGrowthP(Long userId);

    //查询个人用户年增长数
    Result QueryYearGrowthP(Long userId);

    Result QueryPersonalMembers(Long userId);

    List<CompanyTotalDaily>   QueryMemberDailyGrowthP(LocalDateTime start, LocalDateTime end);

    List<CompanyTotalMonth>  QueryMemberMonthGrowthP(String year);

    List<CompanyTotalYear> QueryMemberYearGrowthP();
}

