package com.zx.user.center.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @create 2020-09-27 16:25
 * @description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class OpenIdEntity {

    private String access_token;
    private String expires_in;
    private String refresh_token;
    private String client_id;
    private String openid;
}
