package com.zx.user.center.entity;


import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2020-09-16 18:40:42
 */
@Data
@TableName("acp_user_growth_change")
public class UserGrowthChangeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 成长值改变表id
	 */
	@TableId
	private Long userGrowthId;
	/**
	 * 会员id
	 */
	private Long userId;
	/**
	 * 变化的值
	 */
	private Integer changeCount;
	/**
	 * 备注
	 */
	private String note;
	/**
	 * 来源[0->购物；1->管理员修改]
	 */
	private Integer sourceTyoe;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 状态 0 为删除 1 删除
	 */
	private Integer status;
	/**
	 * 预留字段1
	 */
	private String text1;
	/**
	 * 预留字段2
	 */
	private String text2;
	/**
	 * 预留字段3
	 */
	private String text3;

}
