package com.zx.user.center.entity;


import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2020-09-16 18:40:42
 */
@Data
@TableName("acp_user_receive_address")
public class UserReceiveAddressEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户收货地址表id
	 */
	@TableId
	private Long receiveAddressId;
	/**
	 * 会员id
	 */
	private Long memberId;
	/**
	 * 收货人姓名
	 */
	private String name;
	/**
	 * 电话
	 */
	private String phone;
	/**
	 * 邮政编码
	 */
	private String postCode;
	/**
	 * 省份/直辖市
	 */
	private String province;
	/**
	 * 城市
	 */
	private String city;
	/**
	 * 区
	 */
	private String region;
	/**
	 * 详细地址(街道)
	 */
	private String detailAddress;
	/**
	 * 省市区代码
	 */
	private String areacode;
	/**
	 * 是否默认
	 */
	private Integer defaultStatus;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 状态
	 */
	private Integer status;
	/**
	 * 预留字段1
	 */
	private String text1;
	/**
	 * 预留字段2
	 */
	private String text2;
	/**
	 * 预留字段3
	 */
	private String text3;

}
