package com.zx.user.center.utils;


import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
@Slf4j
public class DateConvert implements Converter<String, LocalDateTime> {
    private String patternal = "yyyy-MM-dd HH:mm:ss";

    @Override
    public LocalDateTime convert(String source) {

//      yyyy-MM-dd hh:mm:ss
        if (source.matches("^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\\s(20|21|22|23|[0-1]\\d):[0-5]\\d:[0-5]\\d")) {
            this.patternal = "yyyy-MM-dd HH:mm:ss";
        }
//         yyyy-MM-dd
        else if (source.matches("^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])")) {
           source = new String(source+" 00:00:00");
        }
//         yyyy-MM-dd hh:mm
        else if (source.matches("^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\\s(20|21|22|23|[0-1]\\d):[0-5]\\d")) {
            source = new String(source+" 00:00:00");
        } else {
            this.patternal = null;
        }

        System.out.println(source);
        if (this.patternal != null) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(this.patternal);
            return LocalDateTime.parse(source, dateTimeFormatter);
        } else {
            log.warn("时间转换失败，即将返回空值，原始时间为{}", source);
            return null;
        }


    }
}
