package com.zx.user.center.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.user.center.dao.UserIntegrationChangeDao;
import com.zx.user.center.entity.UserIntegrationChangeEntity;
import com.zx.user.center.service.UserIntegrationChangeService;
import org.springframework.stereotype.Service;

@Service("userIntegrationChangeService")
public class UserIntegrationChangeServiceImpl extends ServiceImpl<UserIntegrationChangeDao, UserIntegrationChangeEntity> implements UserIntegrationChangeService {


}