package com.zx.user.center.service;

import com.zx.user.center.entity.*;

/**
 * @create 2020-09-21 18:31
 * @description:
 */
public interface LoginOAuth2Service {
    //微博
    UserPersonalCenterEntity login(SocialWeiBoEntity socialWeiBoEntity) throws Exception;

    //支付宝
    UserPersonalCenterEntity login(SocialAlipayEntity socialAlipayEntity) throws Exception;

    //QQ
    UserPersonalCenterEntity login(OpenIdEntity openIdEntity) throws Exception;
    //WeChat
    UserPersonalCenterEntity login(SocialWeChatEntity socialWeChatEntity) throws Exception;

}
