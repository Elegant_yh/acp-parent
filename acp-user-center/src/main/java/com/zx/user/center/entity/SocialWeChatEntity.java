package com.zx.user.center.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @create 2020-09-28 11:00
 * @description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SocialWeChatEntity {

    private String access_token;
    private String expires_in;
    private String refresh_token;
    private String openid;
    private String scope;
    private String unionid;
}
