package com.zx.user.center.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.zx.user.center.config.AlipayConfig;
import com.zx.user.center.dao.UserCompanyCenterDao;
import com.zx.user.center.dao.UserPersonalCenterDao;
import com.zx.user.center.entity.PayOrderEntity;
import com.zx.user.center.entity.UserPersonalCenterEntity;
import com.zx.user.center.service.PayOrderService;
import com.zx.user.center.service.UserCompanyCenterService;
import com.zx.user.center.service.UserPersonalCenterService;
import com.zx.user.center.utils.AlipayOrderIdUtils;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Controller
@Slf4j
public class AlipayController {

    @Autowired
    private UserCompanyCenterService userCompanyCenterService;

    @Autowired
    private AlipayOrderIdUtils alipayOrderIdUtils;

    @Autowired
    private PayOrderService payOrderService;

    @Autowired
    private UserPersonalCenterDao userPersonalCenterDao;

    @Autowired
    private UserCompanyCenterDao userCompanyCenterDao;

    @Autowired
    private UserPersonalCenterService userPersonalCenterService;

    @Autowired
    private UserCompanyCenterService companyUserCenterService;

    /**
     * 即时到账
     * @param payOrderEntity   需要用户登录名 订单金额 订单标题
     * @param request
     * @param response
     * @throws IOException
     */
    @GetMapping("/alipay")
    public void alipay(PayOrderEntity payOrderEntity, HttpServletRequest request, HttpServletResponse response) throws IOException, InterruptedException {
        AlipayClient alipayClient =  new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);  //获得初始化的AlipayClient
        AlipayTradePagePayRequest alipayRequest =  new  AlipayTradePagePayRequest(); //创建API对应的request
        alipayRequest.setReturnUrl(AlipayConfig.return_url );
        alipayRequest.setNotifyUrl( AlipayConfig.notify_url ); //在公共参数中设置回跳和通知地址

        // 生成订单号
        String outTradeNo = alipayOrderIdUtils.alipayOrderCreate();
        payOrderEntity.setOutTradeNo(outTradeNo);
        // 生成订单
        payOrderService.createOrder(payOrderEntity);
        // 请求参数
        // 商户订单号 必填
        String out_trade_no = outTradeNo;
        // 销售产品妈，与支付宝签约的产品码名称， 目前只支持FAST_INSTANT_TRADE_PAY  必填
        String product_code = "FAST_INSTANT_TRADE_PAY";
        // 订单金额 单位为元 精确到小数点后两位 必填
        String total_amount = payOrderEntity.getTotalAmount().toString();
        // 订单标题 必填
        String subject = payOrderEntity.getSubject();

        // 业务参数
        //	请求参数的集合，最大长度不限，除公共参数外所有请求参数都必须放在这个参数中传递，具体参照各产品快速接入文档
        alipayRequest.setBizContent( "{"  +
                "    \"out_trade_no\":\""+out_trade_no+"\","  +
                "    \"product_code\":\""+product_code+"\","  +
                "    \"total_amount\":\""+total_amount+"\","  +
                "    \"subject\":\""+subject+"\","  +
                "    \"timeout_express\":\"15m\""  +
                "  }" ); //填充业务参数
        String form= "" ;
        try  {
            form = alipayClient.pageExecute(alipayRequest).getBody();  //调用SDK生成表单
        }  catch  (AlipayApiException e) {
            e.printStackTrace();
        }
        System.out.println(form);
        response.setContentType( "text/html;charset="  + AlipayConfig.charset);
        response.getWriter().write(form); //直接将完整的表单html输出到页面
        response.getWriter().flush();
        response.getWriter().close();
    }


//    /**
//     * 线下交易查询接口
//     */
//    @GetMapping("/query")
//    public void query(AlipayEntity alipayEntity ,HttpServletResponse response) throws AlipayApiException, IOException {
//        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);  //获得初始化的AlipayClient
//        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
//        //
//        String out_trade_no = alipayEntity.getOut_trade_no();
//        request.setBizContent("{" +
//                "    \"out_trade_no\":\"" + out_trade_no + "\"" +
////                "    \"trade_no\":\"2014112611001004680 073956707\""  +    可选项，但是不可以和out_trade_no同时为空
//                "  }");
//        String queryResponse = alipayClient.execute(request).getBody();
//        response.getWriter().write(queryResponse);
////        if (response.isSuccess()) {
////            System.out.println("调用成功");
////            String body = response.getBody();
//////            System.out.println(body);
////        } else {
////            System.out.println("调用失败");
////        }
//    }


    /**
     * 支付结果同步通知
     * @param request
     * @return
     * @throws AlipayApiException
     */
    @GetMapping("/returnUrl")  // 同步通知需要使用get请求
    public void returnUrl(HttpServletRequest request,HttpServletResponse response) throws AlipayApiException, IOException {
        log.info("同步通知");
        Map<String, String> map = new HashMap<>();
        Map<String, String[]> parameterMap = request.getParameterMap();
        parameterMap.forEach((key, Value)->{
            String valuesStr  = "";
            for (int i =0;i<Value.length;i++){
                valuesStr = (i == Value.length-1)? valuesStr + Value[i] : valuesStr + Value[i]+",";
            }
            map.put(key,valuesStr);
        });

        map.forEach((key, Value)-> System.out.println(key+":"+Value));
        // 调用sdk验证签名
        boolean signVerified = AlipaySignature.rsaCheckV1(map, AlipayConfig.alipay_public_key, AlipayConfig.charset, AlipayConfig.sign_type);
        if (signVerified){ // 验签通过 保存数据到数据库
//            AlipayEntity alipayEntity = new AlipayEntity().setOut_trade_no(map.get("out_tarde_no")).setSubject(map.get("subject")).setTotal_amount(map.get("total_amount"));
//            alipayService.saveOrder(map);
            // 返回支付成功的同步通知页面
            response.getWriter().write("SUCCESS");
        }else {  // 验证失败 返回错误信息
            response.getWriter().write("FAIL");
        }
    }


    /**
     * 支付结果异步通知
     * @param request
     * @param response
     */
    @PostMapping("notifyUrl")
    public void notifyUrl(HttpServletRequest request,HttpServletResponse response) throws AlipayApiException, IOException {
        log.info("异步通知");
        Map<String, String> map = new HashMap<>();
        Map<String, String[]> parameterMap = request.getParameterMap();
        parameterMap.forEach((key, Value)->{
            String valuesStr  = "";
            for (int i =0;i<Value.length;i++){
                valuesStr = (i == Value.length-1)? valuesStr + Value[i] : valuesStr + Value[i]+",";
            }
            map.put(key,valuesStr);
        });
        System.out.println(map.toString());
        // 调用sdk验证签名
        boolean signVerified = AlipaySignature.rsaCheckV1(map, AlipayConfig.alipay_public_key, AlipayConfig.charset, AlipayConfig.sign_type);
        if (signVerified){
            // 验签成功
            // 执行业务代码
            if (map.get("trade_status").equals("TRADE_SUCCESS")){
                // 执行业务代码
                // 充值金额
                BigDecimal balance = BigDecimal.valueOf(Double.valueOf(map.get("total_amount")));
                // 根据订单号获取用户登录名 并修改订单支付状态 ,修改订单的付款时间
                String outTradeNo = map.get("out_trade_no");
                String getPayment = map.get("gmt_payment");
                String loginname = payOrderService.getLoginname(outTradeNo,getPayment);
                // 通过登录名获取对应用户
                if (userPersonalCenterDao.findLoginName(loginname) != null){  // 个人用户
                    // 修改余额
                    userPersonalCenterService.putBalance(loginname, balance);
                }
                if (userCompanyCenterDao.findLoginName(loginname) != null){ // 企业用户
                    // 修改余额
                    companyUserCenterService.putBalance(loginname,balance);
                }

            }

            // 返回success
            response.getWriter().write("success");
        }else response.getWriter().write("fail");
    }

}
































