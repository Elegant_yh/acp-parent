package com.zx.user.center.dao;

import com.zx.user.center.entity.UserPersonalCenterEntity;
import org.apache.ibatis.annotations.Param;

/**
 * @create 2020-09-21 18:33
 * @description:
 */
public interface LoginOAuth2Dao {


    Integer selectCount(String uid);

    UserPersonalCenterEntity selectOne(@Param("uid") String uid);

    void updateById(UserPersonalCenterEntity update);

    void insert(UserPersonalCenterEntity register);
}
