package com.zx.user.center.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author
 * @email
 * @date 2020-09-16 18:40:42
 */
@Data
@TableName("acp_company_user_center")
public class UserCompanyCenterEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId
    private Long userId;
    /**
     * 会员等级id
     */
    private Long levelId;
    /**
     * 企业名/公司名称
     */
    private String username;
    /**
     * 登录名
     */
    private String loginname;
    /**
     * 密码
     */
    private String password;
    /**
     * 座机号码
     */
    private String mobile;
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 联系人
     */
    private String contactPerson;
    /**
     * qq
     */
    private String qq;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 头像
     */
    private String headerImg;
    /**
     * 企业地址
     */
    private String address;
    /**
     * 分组
     */
    private String defaultGroupId;
    /**
     * 个性签名
     */
    private String sign;
    /**
     * 积分
     */
    private Long integration;
    /**
     * 成长值
     */
    private Long growth;
    /**
     * 启用状态（0，未启用，1启用）
     */
    private Integer userStatus;
    /**
     * 是否删除（0，未删除，1，删除）
     */
    private Integer status;
	/**
	 * 营业执照地址
	 */
	private String businessPicture;
    /**
     * 社会信用代码
     */
    private String creditCode;
    /**
     * 经营范围
     */
    private String businessScope;
    /**
     * 成立日期
     */
    private Date establishDate;
    /**
     * 注册资本
     */
    private String registerCapital;
    /**
     * 证件编号
     */
    private String certificcateNumber;
    /**
     * 类型
     */
    private String companyType;
    /**
     * 企业法人
     */
    private String legalPerson;
    /**
     * 有效期至
     */
    private String regEndTime;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 预留字段1
     */
    private String text1;
    /**
     * 预留字段2
     */
    private String text2;
    /**
     * 预留字段3
     */
    private String text3;

}
