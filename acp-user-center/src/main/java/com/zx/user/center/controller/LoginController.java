package com.zx.user.center.controller;

import com.zx.user.center.res.Result;
import com.zx.user.center.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class LoginController {

    @Autowired
    private LoginService loginService;

    /**
     * 登录
     * @param loginname
     * @param password
     * @return
     */
    @PostMapping("/login")
    @ResponseBody
    public Result login(@RequestParam("loginname") String loginname, @RequestParam("password") String password){
        return loginService.login(loginname,password);
    }

    @GetMapping("/loginNameExist")
    @ResponseBody
    public Result loginNameExist(@RequestParam("loginname") String loginname){
        return loginService.loginNameExist(loginname);
    }
}
