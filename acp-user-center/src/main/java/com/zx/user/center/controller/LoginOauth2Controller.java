package com.zx.user.center.controller;


import com.zx.user.center.entity.*;
import com.zx.user.center.res.Result;
import com.zx.user.center.res.ResultCode;
import com.zx.user.center.service.LoginOAuth2Service;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**

 * @create 2020-09-18 15:33
 * @description: 登录controller
 */
@Controller
public class LoginOauth2Controller {

    @Resource
    private LoginOAuth2Service loginService;


    @PostMapping("/oauth2/weibo/login")
    @ResponseBody
    public Result oauthWeiboLogin(@RequestBody SocialWeiBoEntity socialWeiBoEntity) throws Exception {

        UserPersonalCenterEntity entity = loginService.login(socialWeiBoEntity);
        if (entity !=null){
            return new Result(ResultCode.SUCCESS,entity);
        }else {
            return new Result(ResultCode.FAIL);
        }
    }
    @PostMapping("/oauth2/alipay/login")
    @ResponseBody
    Result oauthAlipayLogin(@RequestBody SocialAlipayEntity socialAlipayEntity) throws Exception {

        UserPersonalCenterEntity entity = loginService.login(socialAlipayEntity);
        if (entity !=null){
            return new Result(ResultCode.SUCCESS,entity);
        }else {
            return new Result(ResultCode.FAIL);
        }
    }

    @PostMapping("/oauth2/qq/login")
    @ResponseBody
    Result oauthQQLogin(@RequestBody OpenIdEntity openIdEntity) throws Exception{
        UserPersonalCenterEntity entity = loginService.login(openIdEntity);
        if (entity !=null){
            return new Result(ResultCode.SUCCESS,entity);
        }else {
            return new Result(ResultCode.FAIL);
        }
    }

    @PostMapping("/oauth2/wechat/login")
    @ResponseBody
    Result oauthWeChatLogin(@RequestBody SocialWeChatEntity socialWeChatEntity) throws Exception{
        UserPersonalCenterEntity entity = loginService.login(socialWeChatEntity);
        if (entity !=null){
            return new Result(ResultCode.SUCCESS,entity);
        }else {
            return new Result(ResultCode.FAIL);
        }
    }

}
