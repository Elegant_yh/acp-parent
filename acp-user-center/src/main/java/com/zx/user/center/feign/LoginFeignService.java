package com.zx.user.center.feign;

import com.zx.user.center.entity.SocialWeiBoEntity;
import com.zx.user.center.res.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @create 2020-09-22 15:32
 * @description:
 */
@FeignClient("acp-user-center")
public interface LoginFeignService {

    @PostMapping("/oauth2/login")
    Result oauthLogin(@RequestBody SocialWeiBoEntity socialWeiBoEntity) throws Exception;
}
