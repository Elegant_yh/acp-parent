package com.zx.user.center.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.user.center.entity.MemberLevelEntity;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2020-09-16 18:40:42
 */
public interface MemberLevelService extends IService<MemberLevelEntity> {


}

