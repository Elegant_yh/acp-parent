package com.zx.user.center.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2020-09-16 18:40:42
 */
@Data
@TableName("acp_gitf")
public class GitfEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 礼品表id
	 */
	@TableId
	private Long gitfId;
	/**
	 * 礼品名称
	 */
	private String gitfName;
	/**
	 * 礼品库存
	 */
	private Integer giftStock;
	/**
	 * 兑换礼品所需积分
	 */
	private Integer gitfIntegral;
	/**
	 * 礼品图片
	 */
	private String gitfImg;
	/**
	 * 礼品描述
	 */
	private String gitfDescribes;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 状态 0 未删除 1 已删除  默认0 
	 */
	private Integer status;
	/**
	 * 版本号
	 */
	private String version;
	/**
	 * 预留字段2
	 */
	private String test2;
	/**
	 * 预留字段3
	 */
	private String test3;

}
