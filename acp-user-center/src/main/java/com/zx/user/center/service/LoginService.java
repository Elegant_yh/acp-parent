package com.zx.user.center.service;

import com.zx.user.center.res.Result;

public interface LoginService {

    Result login(String loginname, String password);

    Result loginNameExist(String loginname);
}
