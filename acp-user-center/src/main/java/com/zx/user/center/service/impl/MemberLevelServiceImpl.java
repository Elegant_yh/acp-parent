package com.zx.user.center.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.user.center.dao.MemberLevelDao;
import com.zx.user.center.entity.MemberLevelEntity;
import com.zx.user.center.service.MemberLevelService;
import org.springframework.stereotype.Service;

@Service("memberLevelService")
public class MemberLevelServiceImpl extends ServiceImpl<MemberLevelDao, MemberLevelEntity> implements MemberLevelService {



}