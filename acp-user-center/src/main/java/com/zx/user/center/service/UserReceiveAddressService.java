package com.zx.user.center.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.user.center.entity.UserReceiveAddressEntity;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2020-09-16 18:40:42
 */
public interface UserReceiveAddressService extends IService<UserReceiveAddressEntity> {

}

