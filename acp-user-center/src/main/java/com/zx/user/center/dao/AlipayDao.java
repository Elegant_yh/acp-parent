package com.zx.user.center.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface AlipayDao {

    void saveAlipayOrder();
}
