package com.zx.user.center.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zx.user.center.entity.GitfEntity;
import com.zx.user.center.entity.UserGitfEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2020-09-16 18:40:42
 */
@Repository
@Mapper
public interface GitfDao extends BaseMapper<GitfEntity> {

    /**
     * 查询所有礼品 且库存数量大于0
     * @return
     */
    IPage<GitfEntity> getGitfList(Page<GitfEntity> page, @Param("status") Integer status);

    /**
     * 根据id查询礼品信息
     * @param gitf_id
     * @return
     */
    GitfEntity getGitf(Long gitf_id);

    /**
     * 兑换礼品，库存数量减1
     * @param gitfId
     */
    Integer putGitfById(@Param("gitfId") Long gitfId,@Param("version") Integer version);

    /**
     * 添加礼品
     * @param gitfEntity
     * @return
     */
    Integer postGitf(GitfEntity gitfEntity);

    /**
     * 下架礼品
     * @param gitf
     */
    void deleteGitfById(Long gitf);

    Integer putGitfStockById(@Param("gitf_id") Long gitf_id,@Param("gitf_stock") Integer gitf_stock);
}
