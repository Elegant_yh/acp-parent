package com.zx.user.center.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zx.user.center.entity.CompanyTotalDaily;
import com.zx.user.center.entity.CompanyTotalMonth;
import com.zx.user.center.entity.CompanyTotalYear;
import com.zx.user.center.entity.UserPersonalCenterEntity;
import com.zx.user.center.res.Result;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author
 * @email
 * @date 2020-09-16 18:40:42
 */
@Mapper
@Repository
public interface UserPersonalCenterDao extends BaseMapper<UserPersonalCenterEntity> {

    List<UserPersonalCenterEntity> findAll();

    // 个人注册
    int savePersonal(UserPersonalCenterEntity userPersonalCenterEntity);

    // 根据昵称查询信息
    UserPersonalCenterEntity findLoginName(String loginname);

    // 根据昵称查询数量
    int loginNameCount(String loginname);

    void putBalance(@Param("loginname") String loginname, @Param("balance") BigDecimal balance);

    //查询个人用户数量
    Result QueryNumberOfPersonalUsers(Long userId);

    //查询个人用户日增长数
    Result QueryDailyGrowthP(Long userId);

    //查询个人用户月增长数
    Result QueryMonthGrowthP(Long userId);

    //查询个人用户年增长数
    Result QueryYearGrowthP(Long userId);


    //查询个人用户会员数量
    Result QueryPersonalMembers(Long userId);

    //查询个人用户会员日增长数
    List<CompanyTotalDaily> QueryMemberDailyGrowthP(LocalDateTime start, LocalDateTime end);

    //查询个人用户会员月增长数
    List<CompanyTotalMonth> QueryMemberMonthGrowthP(String year);

    //查询个人用户会员年增长数
    List<CompanyTotalYear> QueryMemberYearGrowthP( );

}
