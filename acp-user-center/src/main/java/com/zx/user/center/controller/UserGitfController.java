package com.zx.user.center.controller;


import com.zx.user.center.entity.UserGitfEntity;
import com.zx.user.center.service.UserGitfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2020-09-16 18:40:42
 */
@RestController
@RequestMapping("center/usergitf")
public class UserGitfController {
    @Autowired
    private UserGitfService userGitfService;

    /**
     * 获取当前用户兑换的礼品的列表
     * @param user_id  当前用户的id
     * @return
     */
    @GetMapping("/list/{user_id}")
    public List<UserGitfEntity> getUserGitfList(@PathVariable(value = "user_id") Long user_id, @RequestParam(value = "status",required = false)Integer status ){

        List<UserGitfEntity> list = userGitfService.getUserGitfList(user_id,status);
        return list;
    }

    /**
     * 用户删除兑换礼品信息
     * @param user_id
     * @param user_gitf_id
     * @return
     */
    @DeleteMapping("/delete/usergitf")
    public String deleteUserGitf(@RequestParam("user_id")Long user_id,@RequestParam("user_gitf_id")Long user_gitf_id){

        String message = userGitfService.deleteUserGitf(user_id,user_gitf_id);
        return  message;
    }

    /**
     * 修改礼品发货状态
     * @param user_gitf_status
     * @return
     */
    @PutMapping("/put/user_gitf_status")
    public String putUserGitfStatus(Integer user_gitf_status,Long user_gitf_id){

        String message = userGitfService.putUserGitfStatus(user_gitf_id,user_gitf_status);
        return null;
    }




}















