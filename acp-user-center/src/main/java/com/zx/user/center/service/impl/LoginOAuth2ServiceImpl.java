package com.zx.user.center.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayUserInfoShareRequest;
import com.alipay.api.response.AlipayUserInfoShareResponse;

import com.zx.user.center.dao.LoginOAuth2Dao;
import com.zx.user.center.entity.*;
import com.zx.user.center.service.LoginOAuth2Service;
import com.zx.user.center.utils.HttpUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @create 2020-09-21 18:31
 * @description:
 */
@Service("LoginService")
public class LoginOAuth2ServiceImpl implements LoginOAuth2Service {

    @Value("${alipay.server_url}")
    private String AL_SERVER_URL;
    @Value("${alipay.app_id}")
    private String AL_APP_ID;
    @Value("${alipay.private_key}")
    private String AL_PRIVATE_KEY;
    @Value("${alipay.public_key}")
    private String AL_PUBLIC_KEY;

    @Resource
    private LoginOAuth2Dao loginDao;


    //微博登录
    @Override
    public UserPersonalCenterEntity login(SocialWeiBoEntity socialWeiBoEntity) throws Exception {
        //登录注册合并逻辑
        String uid = socialWeiBoEntity.getUid();
        //1.判断当前社交用户是否已经登录过系统；
//       Integer social_uid_count = loginDao.selectCount(uid);
        UserPersonalCenterEntity personal = loginDao.selectOne(uid);
        if (personal != null) {
            //这个用户已经注册
            UserPersonalCenterEntity update = new UserPersonalCenterEntity();
            update.setUserId(personal.getUserId());
            update.setAccessToken(socialWeiBoEntity.getAccess_token());
            update.setExpiresIn(socialWeiBoEntity.getExpires_in());

            loginDao.updateById(update);

            personal.setAccessToken(socialWeiBoEntity.getAccess_token());
            personal.setExpiresIn(socialWeiBoEntity.getExpires_in());
            return personal;

        } else {
            //2.没有查询到当前社交用户对应的记录，我们就需要注册一个
            UserPersonalCenterEntity register = new UserPersonalCenterEntity();
            try {
                //3.查询到当前社交用户的用户信息（昵称性别等）
                Map<String, String> query = new HashMap<>();
                Map<String, String> headMap = new HashMap<>();
                query.put("access_token", socialWeiBoEntity.getAccess_token());
                query.put("uid", socialWeiBoEntity.getUid());

                HttpResponse response = HttpUtils.doGet("https://api.weibo.com", "/2/users/show.json", "get", headMap, query);
                if (response.getStatusLine().getStatusCode() == 200) {
                    //查询成功
                    String json = EntityUtils.toString(response.getEntity());
                    JSONObject jsonObject = JSON.parseObject(json);

                    //给用户设置初始化数据
                    String name = jsonObject.getString("name");
                    String gender = jsonObject.getString("gender");
                    String image_url = jsonObject.getString("profile_image_url");
                    String description = jsonObject.getString("description");

                    register.setLoginname(name);
                    register.setGender("m".equals(gender) ? 0 : 1);
                    register.setHeaderImg(image_url);
                    register.setSign(description);

                }
            } catch (Exception e) {
            }
            register.setAccessToken(socialWeiBoEntity.getAccess_token());
            register.setExpiresIn(socialWeiBoEntity.getExpires_in());
            register.setSocialUid(socialWeiBoEntity.getUid());

            register.setLevelId(1l);
            register.setSourceType(4);
            register.setIntegration(10l);
            register.setGrowth(10l);

            loginDao.insert(register);
            return register;
        }
    }

    //Alipay登录
    @Override
    public UserPersonalCenterEntity login(SocialAlipayEntity socialAlipayEntity) throws Exception {

        Thread.sleep(10000);
        //登录注册合并逻辑
        String uid = socialAlipayEntity.getUserId();
        //1.判断当前社交用户是否已经登录过系统；
        UserPersonalCenterEntity personal = loginDao.selectOne(uid);
        if (personal != null) {
            //这个用户已经注册
            UserPersonalCenterEntity update = new UserPersonalCenterEntity();
            update.setUserId(personal.getUserId());
            update.setAccessToken(socialAlipayEntity.getAccessToken());
            update.setExpiresIn(Long.parseLong(socialAlipayEntity.getExpiresTn()));
            update.setRefreshToken(socialAlipayEntity.getRefreshToken());
            update.setReExpiresIn(socialAlipayEntity.getReExpiresTn());

            loginDao.updateById(update);

            personal.setAccessToken(socialAlipayEntity.getAccessToken());
            personal.setExpiresIn(Long.parseLong(socialAlipayEntity.getExpiresTn()));
            personal.setRefreshToken(socialAlipayEntity.getRefreshToken());
            personal.setReExpiresIn(socialAlipayEntity.getReExpiresTn());

            return personal;

        } else {
            //2.没有查询到当前社交用户对应的记录，我们就需要注册一个
            UserPersonalCenterEntity register = new UserPersonalCenterEntity();
            try {
                //3.查询到当前社交用户的用户信息（昵称性别等）
                AlipayClient alipayClient = new DefaultAlipayClient(AL_SERVER_URL, AL_APP_ID, AL_PRIVATE_KEY, "json", "UTF-8", AL_PUBLIC_KEY, "RSA2");

                AlipayUserInfoShareRequest requestUser = new AlipayUserInfoShareRequest();
                AlipayUserInfoShareResponse userInfoResponse = alipayClient.execute(requestUser, socialAlipayEntity.getAccessToken());
                //查询成功
                String json = userInfoResponse.getBody();
                JSONObject jsonObject = JSON.parseObject(json);
                String response = jsonObject.getString("alipay_user_info_share_response");
                JSONObject jsonObject1 = JSON.parseObject(response);
                //给用户设置初始化数据
                String name = jsonObject1.getString("nick_name");
                String gender = jsonObject1.getString("gender");
                String image_url = jsonObject1.getString("avatar");

                register.setLoginname(name);
                register.setGender("M".equals(gender) ? 0 : 1);
                register.setHeaderImg(image_url);


            } catch (Exception e) {
            }
            register.setAccessToken(socialAlipayEntity.getAccessToken());
            register.setExpiresIn(Long.parseLong(socialAlipayEntity.getExpiresTn()));
            register.setRefreshToken(socialAlipayEntity.getRefreshToken());
            register.setReExpiresIn(socialAlipayEntity.getReExpiresTn());
            register.setSocialUid(socialAlipayEntity.getUserId());

            register.setLevelId(1l);
            register.setSourceType(4);
            register.setIntegration(10l);
            register.setGrowth(10l);

            loginDao.insert(register);
            return register;
        }
    }

    //QQ登录
    @Override
    public UserPersonalCenterEntity login(OpenIdEntity openIdEntity) throws Exception {
        //登录注册合并逻辑
        String uid = openIdEntity.getOpenid();
        //1.判断当前社交用户是否已经登录过系统；
//       Integer social_uid_count = loginDao.selectCount(uid);
        UserPersonalCenterEntity personal = loginDao.selectOne(uid);
        if (personal != null) {
            //这个用户已经注册
            UserPersonalCenterEntity update = new UserPersonalCenterEntity();
            update.setUserId(personal.getUserId());
            update.setAccessToken(openIdEntity.getAccess_token());
            update.setExpiresIn(Long.parseLong(openIdEntity.getExpires_in()));
            update.setRefreshToken(openIdEntity.getRefresh_token());

            loginDao.updateById(update);

            personal.setAccessToken(openIdEntity.getAccess_token());
            personal.setExpiresIn(Long.parseLong(openIdEntity.getExpires_in()));
            personal.setRefreshToken(openIdEntity.getRefresh_token());
            return personal;

        } else {
            //2.没有查询到当前社交用户对应的记录，我们就需要注册一个
            UserPersonalCenterEntity register = new UserPersonalCenterEntity();
            try {
                //3.查询到当前社交用户的用户信息（昵称性别等）
                Map<String, String> reqHeader = new HashMap<>();
                Map<String, String> reqQuery = new HashMap<>();

                Map<String, String> reqMap = new HashMap<>();
                reqMap.put("access_token", openIdEntity.getAccess_token());
                reqMap.put("oauth_consumer_key", openIdEntity.getClient_id());
                reqMap.put("openid", openIdEntity.getOpenid());

                HttpResponse response = HttpUtils.doPost("https://graph.qq.com", "/user/get_user_info", "get", reqHeader, reqQuery, reqMap);
                if (response.getStatusLine().getStatusCode() == 200) {
                    //查询成功
                    String json = EntityUtils.toString(response.getEntity());
                    JSONObject jsonObject = JSON.parseObject(json);
                    //给用户设置初始化数据
                    String name = jsonObject.getString("nickname");
                    String gender = jsonObject.getString("gender");
                    String image_url = jsonObject.getString("figureurl_qq");

                    register.setLoginname(name);
                    register.setGender("男".equals(gender) ? 0 : 1);
                    register.setHeaderImg(image_url);


                }
            } catch (Exception e) {
            }
            register.setAccessToken(openIdEntity.getAccess_token());
            register.setExpiresIn(Long.parseLong(openIdEntity.getExpires_in()));
            register.setRefreshToken(openIdEntity.getRefresh_token());

            register.setLevelId(1l);
            register.setSourceType(4);
            register.setIntegration(10l);
            register.setGrowth(10l);

            loginDao.insert(register);
            return register;
        }

    }

    @Override
    public UserPersonalCenterEntity login(SocialWeChatEntity socialWeChatEntity) throws Exception {
        //登录注册合并逻辑
        String uid = socialWeChatEntity.getOpenid();
        //1.判断当前社交用户是否已经登录过系统；
        UserPersonalCenterEntity personal = loginDao.selectOne(uid);
        if (personal != null) {
            //这个用户已经注册
            UserPersonalCenterEntity update = new UserPersonalCenterEntity();
            update.setUserId(personal.getUserId());
            update.setAccessToken(socialWeChatEntity.getAccess_token());
            update.setExpiresIn(Long.parseLong(socialWeChatEntity.getExpires_in()));

            loginDao.updateById(update);

            personal.setAccessToken(socialWeChatEntity.getAccess_token());
            personal.setExpiresIn(Long.parseLong(socialWeChatEntity.getExpires_in()));
            return personal;

        } else {
            //2.没有查询到当前社交用户对应的记录，我们就需要注册一个
            UserPersonalCenterEntity register = new UserPersonalCenterEntity();
            try {
                //3.查询到当前社交用户的用户信息（昵称性别等）
                Map<String, String> query = new HashMap<>();
                Map<String, String> headMap = new HashMap<>();
                query.put("access_token", socialWeChatEntity.getAccess_token());
                query.put("openid", socialWeChatEntity.getOpenid());

                HttpResponse response = HttpUtils.doGet("https://api.weixin.qq.com", "/sns/userinfo", "get", headMap, query);
                if (response.getStatusLine().getStatusCode() == 200) {
                    //查询成功
                    String json = EntityUtils.toString(response.getEntity());
                    JSONObject jsonObject = JSON.parseObject(json);

                    //给用户设置初始化数据
                    String name = jsonObject.getString("nickname");
                    String gender = jsonObject.getString("sex");
                    String image_url = jsonObject.getString("headimgurl");

                    register.setLoginname(name);
                    register.setGender("1".equals(gender) ? 0 : 1);
                    register.setHeaderImg(image_url);

                }
            } catch (Exception e) {
            }
            register.setAccessToken(socialWeChatEntity.getAccess_token());
            register.setExpiresIn(Long.parseLong(socialWeChatEntity.getExpires_in()));
            register.setSocialUid(socialWeChatEntity.getOpenid());

            register.setLevelId(1l);
            register.setSourceType(4);
            register.setIntegration(10l);
            register.setGrowth(10l);

            loginDao.insert(register);
            return register;
        }
    }
}
