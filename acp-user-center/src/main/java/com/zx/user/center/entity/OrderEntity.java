package com.zx.user.center.entity;


import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2020-09-16 18:40:42
 */
@Data
@TableName("acp_order")
public class OrderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 订单表id
	 */
	@TableId
	private Long orderId;
	/**
	 * 订单号
	 */
	private Long orderSn;
	/**
	 * 用户id
	 */
	private Long orderUserId;
	/**
	 * 用户名
	 */
	private String orderUserName;
	/**
	 * 订单总额
	 */
	private Double orderTotal;
	/**
	 * 使用的
使用的优惠券
	 */
	private String orderCoupon;
	/**
	 * 优惠金额
	 */
	private Double orderDiscount;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 支付方式
	 */
	private String orderPayType;
	/**
	 * 状态 0 为删除 1 已删除 默认0
	 */
	private Integer status;
	/**
	 * 获得积分
	 */
	private Integer orderIntegral;
	/**
	 * 活动主题
	 */
	private String orderActivitySub;
	/**
	 * 活动海报
	 */
	private String orderActivityImg;
	/**
	 * 预留字段1
	 */
	private String test1;
	/**
	 * 预留字段2
	 */
	private String test2;
	/**
	 * 预留字段3
	 */
	private String test3;

}
