package com.zx.user.center.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zx.user.center.entity.UserGitfEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2020-09-16 18:40:42
 */
@Mapper
@Repository
public interface UserGitfDao extends BaseMapper<UserGitfEntity> {


    List<UserGitfEntity> getUserGitfList(@Param("user_id") Long user_id,@Param("status") Integer status);

    /**
     * 新增用户兑换的礼品信息
     */
    void postUserGitf(UserGitfEntity userGitfEntity);

    Integer deleteUserGitf(Long user_id, Long user_gitf_id);

    Integer putUserGitfStatus(@Param("user_gitf_id") Long user_gitf_id,@Param("user_gitf_status") Integer user_gitf_status);
}
