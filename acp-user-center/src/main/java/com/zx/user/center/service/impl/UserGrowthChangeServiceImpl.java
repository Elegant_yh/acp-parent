package com.zx.user.center.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.user.center.dao.UserGrowthChangeDao;
import com.zx.user.center.entity.UserGrowthChangeEntity;
import com.zx.user.center.service.UserGrowthChangeService;
import org.springframework.stereotype.Service;

@Service("userGrowthChangeService")
public class UserGrowthChangeServiceImpl extends ServiceImpl<UserGrowthChangeDao, UserGrowthChangeEntity> implements UserGrowthChangeService {



}