package com.zx.user.center.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @create 2020-09-23 18:39
 * @description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SocialAlipayEntity {

    private String appId;
    private String userId;
    private String accessToken;
    private String refreshToken;
    private String expiresTn;
    private String reExpiresTn;

}
