package com.zx.mapper;

import com.zx.model.AcpOnline;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author MyBatisPlusGenerater
 * @since 2020-09-17
 */
@Repository
public interface AcpOnlineMapper extends BaseMapper<AcpOnline> {

}
