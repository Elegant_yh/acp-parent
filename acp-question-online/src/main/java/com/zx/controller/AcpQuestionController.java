package com.zx.controller;


import com.zx.model.AcpQuestion;
import com.zx.service.AcpQuestionService;
import com.zx.util.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author MyBatisPlusGenerater
 * @since 2020-09-17
 */
@Api(tags="常见问题")
@Slf4j
@Controller
@RequestMapping("//acp-question")
public class AcpQuestionController {
    @Autowired
    private  AcpQuestionService acpQuestionService;



    /**
     *
     * @param model 对页面传值
     * @return
     */
    @ApiOperation(value = "查询所有问题")
    @RequestMapping("/findAll")
    public String findAll(Model model){
        List<AcpQuestion> acpQuestionList =acpQuestionService.findAll();
        model.addAttribute("acpQuestionList",acpQuestionList);
        return "oftenQuestion";
    }

    @ApiOperation(value="提出问题")
    @RequestMapping("/comeUpWith")
    public CommonResult comeUpWith(AcpQuestion acpQuestion){
        Integer i = acpQuestionService.comeUpWith(acpQuestion);
        if(i>0){
            return CommonResult.ok("提出成功");
        }else{
            return CommonResult.error();
        }
    }

    /**
     *
     * 获取页面某个挂载问题的回复
     * @param id
     * @param model
     * @return
     */
    @ApiOperation(value="获取答案")
    @RequestMapping("/getAnswerByQuestionId")
    public String getAnswerByQuestionId(@RequestParam("id") Integer id ,Model model){
        AcpQuestion acpQuestion= acpQuestionService.getAnswerByQuestionId(id);
        model.addAttribute("question",acpQuestion);
        return "answer";
    }
}
