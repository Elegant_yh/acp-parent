package com.zx.controller;

import com.zx.model.AcpOnline;
import com.zx.service.AcpOnlineService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author MyBatisPlusGenerater
 * @since 2020-09-17
 */
@Api(tags = "联系我们 在线客服")
@Controller
@RequestMapping("//acp-online")
public class AcpOnlineController {
    @Autowired
    private AcpOnlineService acpOnlineService;

    @ApiOperation("查询联系方式")
    @RequestMapping("/getContact")
    public String getConstract(Model model){
       AcpOnline acpOnline=acpOnlineService.getContact();
       model.addAttribute("acpOfflineContact",acpOnline);
        return "contactUs";
    }
}
