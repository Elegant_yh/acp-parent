package com.zx.service;

import com.zx.model.AcpOnline;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author MyBatisPlusGenerater
 * @since 2020-09-17
 */
public interface AcpOnlineService extends IService<AcpOnline> {

    AcpOnline getContact();
}
