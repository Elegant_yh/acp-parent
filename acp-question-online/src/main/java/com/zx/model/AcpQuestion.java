package com.zx.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author MyBatisPlusGenerater
 * @since 2020-09-17
 */
@TableName("acp_question")
public class AcpQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 常见问题主键
     */
    @TableId(value = "question_id", type = IdType.AUTO)
    private Long questionId;

    /**
     * 常见问题内容
     */
    @TableField("question_context")
    private String questionContext;

    /**
     * 问题是否回复 默认0，0未答复，1已答复
     */
    @TableField("question_answer_statu")
    private Long questionAnswerStatu;

    /**
     * 问题回复内容
     */
    @TableField("question_answer")
    private String questionAnswer;

    /**
     * 问题答复时间
     */
    @TableField("question_answer_time")
    private LocalDateTime questionAnswerTime;

    /**
     * 问题提交人
     */
    @TableField("question_submitter")
    private String questionSubmitter;

    /**
     * 逻辑删除状态 默认值0， 0未删除，1删除
     */
    @TableField("status")
    private Integer status;

    /**
     * 问题创建时间
     */
    @TableField("question_create_time")
    private LocalDateTime questionCreateTime;

    /**
     * 问题答复人
     */
    @TableField("question_answer_person")
    private String questionAnswerPerson;


    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getQuestionContext() {
        return questionContext;
    }

    public void setQuestionContext(String questionContext) {
        this.questionContext = questionContext;
    }

    public Long getQuestionAnswerStatu() {
        return questionAnswerStatu;
    }

    public void setQuestionAnswerStatu(Long questionAnswerStatu) {
        this.questionAnswerStatu = questionAnswerStatu;
    }

    public String getQuestionAnswer() {
        return questionAnswer;
    }

    public void setQuestionAnswer(String questionAnswer) {
        this.questionAnswer = questionAnswer;
    }

    public LocalDateTime getQuestionAnswerTime() {
        return questionAnswerTime;
    }

    public void setQuestionAnswerTime(LocalDateTime questionAnswerTime) {
        this.questionAnswerTime = questionAnswerTime;
    }

    public String getQuestionSubmitter() {
        return questionSubmitter;
    }

    public void setQuestionSubmitter(String questionSubmitter) {
        this.questionSubmitter = questionSubmitter;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getQuestionCreateTime() {
        return questionCreateTime;
    }

    public void setQuestionCreateTime(LocalDateTime questionCreateTime) {
        this.questionCreateTime = questionCreateTime;
    }

    public String getQuestionAnswerPerson() {
        return questionAnswerPerson;
    }

    public void setQuestionAnswerPerson(String questionAnswerPerson) {
        this.questionAnswerPerson = questionAnswerPerson;
    }

    @Override
    public String toString() {
        return "AcpQuestion{" +
                "questionId=" + questionId +
                ", questionContext=" + questionContext +
                ", questionAnswerStatu=" + questionAnswerStatu +
                ", questionAnswer=" + questionAnswer +
                ", questionAnswerTime=" + questionAnswerTime +
                ", questionSubmitter=" + questionSubmitter +
                ", status=" + status +
                ", questionCreateTime=" + questionCreateTime +
                ", questionAnswerPerson=" + questionAnswerPerson +
                "}";
    }
}
