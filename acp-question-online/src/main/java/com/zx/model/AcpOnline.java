package com.zx.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author MyBatisPlusGenerater
 * @since 2020-09-17
 */
@TableName("acp_online")
public class AcpOnline implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "online_id", type = IdType.AUTO)
    private Long onlineId;

    /**
     * 联系人
     */
    @TableField("online_contacts")
    private String onlineContacts;

    /**
     * 联系电话
     */
    @TableField("online_telephone")
    private String onlineTelephone;

    /**
     * 邮箱
     */
    @TableField("online_mailbox")
    private String onlineMailbox;

    /**
     * 招聘邮箱
     */
    @TableField("online_recruitment_email")
    private String onlineRecruitmentEmail;

    /**
     * 招聘电话
     */
    @TableField("online_recruitment_telephone")
    private String onlineRecruitmentTelephone;

    /**
     * 传真
     */
    @TableField("online_fax")
    private String onlineFax;

    /**
     * 联系地址
     */
    @TableField("online_contact_address")
    private String onlineContactAddress;


    public Long getOnlineId() {
        return onlineId;
    }

    public void setOnlineId(Long onlineId) {
        this.onlineId = onlineId;
    }
    public String getOnlineContacts() {
        return onlineContacts;
    }

    public void setOnlineContacts(String onlineContacts) {
        this.onlineContacts = onlineContacts;
    }
    public String getOnlineTelephone() {
        return onlineTelephone;
    }

    public void setOnlineTelephone(String onlineTelephone) {
        this.onlineTelephone = onlineTelephone;
    }
    public String getOnlineMailbox() {
        return onlineMailbox;
    }

    public void setOnlineMailbox(String onlineMailbox) {
        this.onlineMailbox = onlineMailbox;
    }
    public String getOnlineRecruitmentEmail() {
        return onlineRecruitmentEmail;
    }

    public void setOnlineRecruitmentEmail(String onlineRecruitmentEmail) {
        this.onlineRecruitmentEmail = onlineRecruitmentEmail;
    }
    public String getOnlineRecruitmentTelephone() {
        return onlineRecruitmentTelephone;
    }

    public void setOnlineRecruitmentTelephone(String onlineRecruitmentTelephone) {
        this.onlineRecruitmentTelephone = onlineRecruitmentTelephone;
    }
    public String getOnlineFax() {
        return onlineFax;
    }

    public void setOnlineFax(String onlineFax) {
        this.onlineFax = onlineFax;
    }
    public String getOnlineContactAddress() {
        return onlineContactAddress;
    }

    public void setOnlineContactAddress(String onlineContactAddress) {
        this.onlineContactAddress = onlineContactAddress;
    }
    @Override
    public String toString() {
        return "AcpOnline{" +
            "onlineId=" + onlineId +
            ", onlineContacts=" + onlineContacts +
            ", onlineTelephone=" + onlineTelephone +
            ", onlineMailbox=" + onlineMailbox +
            ", onlineRecruitmentEmail=" + onlineRecruitmentEmail +
            ", onlineRecruitmentTelephone=" + onlineRecruitmentTelephone +
            ", onlineFax=" + onlineFax +
            ", onlineContactAddress=" + onlineContactAddress +

        "}";
    }
}
