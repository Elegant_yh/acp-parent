package com.zx.auth.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author
 * @email
 * @date 2020-09-16 18:40:42
 */
@ToString
@Data
@TableName("acp_personal_user_center")
public class UserPersonalCenterEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId
    private Long userId;
    /**
     * 会员等级
     */
    private Long levelId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 昵称
     */
    private String loginname;
    /**
     * 手机号码
     */
    private String mobile;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 头像
     */
    private String headerImg;
    /**
     * 性别
     */
    private Integer gender;
    /**
     * 生日
     */
    private String birth;
    /**
     * 用户地址
     */
    private String address;
    /**
     * 民族
     */
    private String minZu;
    /**
     * 个性签名
     */
    private String sign;
    /**
     * 用户来源（1平台注册，2支付宝，3微信，4微博，5qq）
     */
    private Integer sourceType;
    /**
     * 积分
     */
    private Long integration;
    /**
     * 成长值
     */
    private Long growth;
    /**
     * 启用状态（0，未启用，1启用）
     */
    private Integer userStatus;
    /**
     * 身份正面图片地址
     */
    private String identityFrontImage;
    /**
     * 身份背面图片地址
     */
    private String identityBackImage;
    /**
     * 人脸识别图片地址
     */
    private String faceRecognitionImage;
    /**
     * 身份证号
     */
    private Long identityNumber;
    /**
     * 是否删除（0，未删除，1，删除）
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;

    /*
    社交用户唯一id
     */
    private String socialUid;
    /*
    访问令牌
     */
    private String accessToken;
    /*
    访问令牌过期时间
     */
    private Long expiresIn;
    /*
    刷新令牌
     */
    private String refreshToken;
    /*
    刷新令牌有效期
     */
    private String reExpiresIn;

    /**
     * 预留字段1
     */
    private String text1;
    /**
     * 预留字段2
     */
    private String text2;
    /**
     * 预留字段3
     */
    private String text3;

}
