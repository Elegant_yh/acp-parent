/**
  * Copyright 2020 bejson.com 
  */
package com.zx.auth.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SocialQQEntity {

    private String access_token;
    private String expires_in;
    private String refresh_token;


}