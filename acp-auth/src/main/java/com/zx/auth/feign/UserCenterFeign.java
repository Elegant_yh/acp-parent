package com.zx.auth.feign;

import com.zx.auth.entity.UserCompanyCenterEntity;
import com.zx.auth.entity.UserPersonalCenterEntity;
import com.zx.auth.feign.fallback.UserCenterFeignFallback;
import com.zx.auth.res.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 调用第三方服务
 */
@Component
@FeignClient(name = "acp-user-center", fallback = UserCenterFeignFallback.class)
public interface UserCenterFeign {

    // 个人注册
    @PostMapping("/personal/savePersonal")
    Result savePersonal(@RequestBody UserPersonalCenterEntity userPersonalCenterEntity);

    // 企业注册
    @PostMapping("/company/saveCompany")
    Result saveCompany(@RequestBody UserCompanyCenterEntity userCompanyCenterEntity);

    // 登录
    @PostMapping("/login")
    Result login(@RequestParam("loginname") String loginname, @RequestParam("password") String password);

    // 判断昵称是否存在
    @GetMapping("/loginNameExist")
    Result loginNameExist(@RequestParam("loginname") String loginname);
}
