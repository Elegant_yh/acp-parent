package com.zx.auth.feign.fallback;

import com.zx.auth.entity.UserCompanyCenterEntity;
import com.zx.auth.entity.UserPersonalCenterEntity;
import com.zx.auth.feign.UserCenterFeign;
import com.zx.auth.res.Result;
import org.springframework.stereotype.Component;

@Component
public class UserCenterFeignFallback implements UserCenterFeign {
    @Override
    public Result savePersonal(UserPersonalCenterEntity userPersonalCenterEntity) {
        return null;
    }

    @Override
    public Result saveCompany(UserCompanyCenterEntity userCompanyCenterEntity) {
        return null;
    }

    @Override
    public Result login(String loginname, String password) {
        return null;
    }

    @Override
    public Result loginNameExist(String loginname) {
        return null;
    }
}
