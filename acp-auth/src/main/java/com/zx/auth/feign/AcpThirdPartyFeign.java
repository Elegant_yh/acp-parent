package com.zx.auth.feign;

import com.zx.auth.feign.fallback.AcpThirdPartyFeignFallback;
import com.zx.auth.res.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 * 调用第三方服务
 */
@Component
@FeignClient(name = "acp-thirdparty" ,fallback = AcpThirdPartyFeignFallback.class)
public interface AcpThirdPartyFeign {

    // 身份证识别
    @PostMapping(value = "/ocr/idCard" , consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    Result idCard(@RequestPart(value = "file", required = false) MultipartFile file, @RequestParam("idCardSide") String idCardSide);

    // 银行卡识别
    @PostMapping(value = "/ocr/bankCard", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    Result bankCard(@RequestPart(value = "file", required = false) MultipartFile file);

    // 营业执照识别
    @PostMapping(value = "/ocr/businessLicense", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    Result businessLicense(@RequestPart(value = "file", required = false) MultipartFile file);

    // 短信验证码
    @PostMapping("/ali/sendSms")
    void sendSms(@RequestParam("mobile") String mobile, @RequestParam("code") String code);
}
