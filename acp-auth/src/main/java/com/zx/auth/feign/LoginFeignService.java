package com.zx.auth.feign;


import com.zx.auth.entity.*;
import com.zx.auth.feign.fallback.LoginFeignFallback;
import com.zx.auth.res.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @create 2020-09-22 15:32
 * @description:
 */
@FeignClient(value = "acp-user-center",fallback = LoginFeignFallback.class)
public interface LoginFeignService {

    @PostMapping("/oauth2/weibo/login")
    @ResponseBody
    Result oauthWeiboLogin(@RequestBody SocialWeiBoEntity socialWeiBoEntity) throws Exception;

    @PostMapping("/oauth2/alipay/login")
    @ResponseBody
    Result oauthAlipayLogin(@RequestBody SocialAlipayEntity socialAlipayEntity) throws Exception;


    @PostMapping("/oauth2/qq/login")
    @ResponseBody
    Result oauthQQLogin(@RequestBody OpenIdEntity openIdEntity) throws Exception;

    @PostMapping("/oauth2/wechat/login")
    @ResponseBody
    Result oauthWeChatLogin(@RequestBody SocialWeChatEntity socialWeChatEntity) throws Exception;
}
