package com.zx.auth.app;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class forwardController {

    @RequestMapping("/login")
    public String login(){

        return "login";
    }

    @RequestMapping("/login/personal")
    public String showPersonal(){

        return "login/register/personal-register";
    }

    @RequestMapping("/login/company")
    public String showCompany(){

        return "login/register/company-register";
    }

    @RequestMapping("/success")
    public String success(){

        return "success";
    }
    @RequestMapping("/login/faceLogin")
    public String faceLogin(){

        return "faceLogin";
    }
    @RequestMapping("/login/faceReg")
    public String faceRes(){

        return "faceReg.html";
    }

}
