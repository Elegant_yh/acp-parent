package com.zx.auth.controller;

import com.zx.auth.entity.UserCompanyCenterEntity;
import com.zx.auth.entity.UserPersonalCenterEntity;
import com.zx.auth.feign.UserCenterFeign;
import com.zx.auth.res.Result;
import com.zx.auth.res.ResultCode;
import com.zx.auth.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @description: 注册controller
 */
@Controller
@RequestMapping("/register")
public class RegisterController {
    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private UserCenterFeign userCenterFeign;

    /**
     * 判断昵称是否重复
     * @param loginname
     * @return
     */
    @GetMapping("/loginNameExist")
    @ResponseBody
    public Result loginNameExist(@RequestParam("loginname") String loginname){
        return userCenterFeign.loginNameExist(loginname);
    }

    /**
     * 个人注册
     *
     * @return
     */
    @PostMapping(value = "/personal")
    @ResponseBody
    public Result registerPersonal(@RequestBody UserPersonalCenterEntity userPersonalCenterEntity,
                                   @RequestParam("code") String code) {
        // 获取redis中的验证码
        String phoneCode = redisUtil.get(userPersonalCenterEntity.getMobile());

        if (phoneCode == "") {
            return new Result(ResultCode.FAIL, "验证码已过期");
        } else if (!phoneCode.equals(code)) {
            return new Result(ResultCode.FAIL, "验证码错误");
        }
        // 判断是否重复提交
        String key = "phone:" + userPersonalCenterEntity.getMobile();

        if (!redisUtil.hasKey(key)) {
            redisUtil.setTime(key, "true");

            Result result = userCenterFeign.savePersonal(userPersonalCenterEntity);
            redisUtil.deleteKey(key);

            return result;
        }
        return new Result(ResultCode.FAIL, "请勿重复提交");
    }

    /**
     * 企业注册
     *
     * @return
     */
    @PostMapping(value = "/company")
    @ResponseBody
    public Result companyPersonal(@RequestBody UserCompanyCenterEntity userCompanyCenterEntity,
                                  @RequestParam("code") String code) {
        // 获取redis中的验证码
        String phoneCode = redisUtil.get(userCompanyCenterEntity.getPhone());

        if (phoneCode == "") {
            return new Result(ResultCode.FAIL, "验证码已过期");

        } else if (!phoneCode.equals(code)) {
            return new Result(ResultCode.FAIL, "验证码错误");
        }

        // 判断是否重复提交
        String key = "phone:" + userCompanyCenterEntity.getPhone();
        if (!redisUtil.hasKey(key)) {
            redisUtil.setTime(key, "true");

            Result result = userCenterFeign.saveCompany(userCompanyCenterEntity);
            redisUtil.deleteKey(key);

            return result;
        }
        return new Result(ResultCode.FAIL, "请勿重复提交");
    }
}
