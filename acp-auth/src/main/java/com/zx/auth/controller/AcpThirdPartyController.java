package com.zx.auth.controller;

import com.alibaba.fastjson.JSONObject;
import com.zx.auth.feign.AcpThirdPartyFeign;
import com.zx.auth.res.Result;
import com.zx.auth.res.ResultCode;
import com.zx.auth.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Random;

@Controller
@RequestMapping("/third-party")
public class AcpThirdPartyController {

    @Autowired
    private AcpThirdPartyFeign acpThirdPartyFeign;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private FileController fileController;

    /**
     * 身份证识别
     *
     * @param file
     * @param idCardSide
     * @return
     * @throws IOException
     */
    @PostMapping("/idCard")
    @ResponseBody
    public Result idCard(@RequestPart(value = "file", required = false) MultipartFile file,
                         @RequestParam("idCardSide") String idCardSide) {
        try {
            Result result = acpThirdPartyFeign.idCard(file, idCardSide);
            if (result.getCode() != 200)
                return new Result(ResultCode.FAIL, "身份证识别失败");
            String imgPath = fileController.uploadImg(file);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("words_result", result.getData());
            jsonObject.put("imgPath", imgPath);
            return new Result(ResultCode.SUCCESS, "上传成功", jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(ResultCode.FAIL, "上传异常，请重新上传");
        }
    }

    /**
     * 银行卡识别
     *
     * @param file
     * @return
     */
    @PostMapping("/bankCard")
    @ResponseBody
    public Result bankCard(@RequestPart(value = "file", required = false) MultipartFile file) {
        try {
            Result result = acpThirdPartyFeign.bankCard(file);
            if (result.getCode() != 200)
                return new Result(ResultCode.FAIL, "银行卡识别失败");
            String imgPath = fileController.uploadImg(file);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("words_result", result.getData());
            jsonObject.put("imgPath", imgPath);
            return new Result(ResultCode.SUCCESS, "上传成功", jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(ResultCode.FAIL, "上传异常，请重新上传");
        }
    }

    /**
     * 营业执照识别
     *
     * @param file
     * @return
     */
    @PostMapping("/businessLicense")
    @ResponseBody
    public Result businessLicense(@RequestPart(value = "file", required = false) MultipartFile file) {
        try {
            Result result = acpThirdPartyFeign.businessLicense(file);
            if (result.getCode() != 200)
                return new Result(ResultCode.FAIL, "营业执照识别失败");
            String imgPath = fileController.uploadImg(file);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("words_result", result.getData());
            jsonObject.put("imgPath", imgPath);
            return new Result(ResultCode.SUCCESS, "上传成功", jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(ResultCode.FAIL, "上传异常，请重新上传");
        }
    }

    /**
     * 发送验证码
     *
     * @param mobile 手机号
     */
    @PostMapping("/sendSMS")
    @ResponseBody
    public void sendSMS(@RequestParam String mobile) {
        // 生成随机六位数验证码
        String code = String.valueOf(new Random().nextInt(899999) + 100000);
        redisUtil.setTime(mobile, code);
        acpThirdPartyFeign.sendSms(mobile, code);
    }

}
