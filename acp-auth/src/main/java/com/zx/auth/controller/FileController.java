package com.zx.auth.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

@Controller
@RequestMapping("/file")
public class FileController {

    @Value("${file.uploadPath}")
    private String uploadPath;

    @Value("${server.port}")
    private String port;

    @PostMapping("/upload/img")
    public String uploadImg(@RequestParam(value = "file", required = false) MultipartFile file) throws IOException {

        // 判断文件夹是否存在，不存在则创建
        File filePath = new File(uploadPath);

        if (!filePath.exists()){
            filePath.mkdirs();
        }
        // 获取文件名
        String filename = file.getOriginalFilename();
        // 存储文件的路径
        String path = uploadPath+filename;
        File files = new File(path);
        file.transferTo(files);
        InetAddress ipAddress = InetAddress.getLocalHost();
        // 返回图片地址
        return "/img/"+filename;
    }

    @PostMapping("/upload/imgLayuiEdit")
    @ResponseBody
    public String imgLayuiEdit(@RequestParam(value = "file", required = false) MultipartFile file) throws IOException {
        // 判断文件夹是否存在，不存在则创建
        File filePath = new File(uploadPath);
        if (!filePath.exists()){
            filePath.mkdirs();
        }
        // 获取文件名
        String filename = file.getOriginalFilename();
        // 存储文件的路径
        String path = uploadPath+filename;
        File files = new File(path);
        file.transferTo(files);
        // 返回图片地址
        JSONObject object = new JSONObject();
        JSONObject json = new JSONObject();
        json.put("src","http://localhost:9900/acp-auth/img/"+filename);
        json.put("title",filename);
        object.put("code",0);
        object.put("msg","上传成功");
        object.put("data",json);
        return object.toString();
    }
}
