package com.zx.auth.controller;

import com.zx.auth.feign.UserCenterFeign;
import com.zx.auth.res.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {

    @Autowired
    private UserCenterFeign userCenterFeign;

    /**
     * 登录
     * @param loginname
     * @param password
     * @return
     */
    @PostMapping("/login")
    @ResponseBody
    public Result login(@RequestParam("loginname") String loginname, @RequestParam("password") String password){
        return userCenterFeign.login(loginname,password);
    }
}
