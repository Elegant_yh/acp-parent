package com.zx.auth.service;
import com.zx.auth.config.BaiduFaceConfig;
import com.zx.auth.utils.BaiduAIFaceUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

@Service
@Slf4j
public class FaceLoginService {

    @Autowired
    BaiduAIFaceUtil baiduAIFaceUtil;
    @Autowired
    BaiduFaceConfig baiduFaceConfig;
    public Map<String,Object> searchface(StringBuffer imagebase64) throws IOException {
        String substring = imagebase64.substring(imagebase64.indexOf(",")+1, imagebase64.length());
        baiduFaceConfig.setImgpath(substring);
        baiduFaceConfig.setGroupID("test");
//        System.out.println(substring);
        baiduFaceConfig.setLiveness_Control("NONE");
        baiduFaceConfig.setQuality_Control("NONE");
        Map check = baiduAIFaceUtil.FaceCheck(baiduFaceConfig);
        log.info("得到的check为{}:"+check);
//        if (check!=null && check.get("error_msg") == "SUCCESS"){
//            Map map = baiduAIFaceUtil.FaceSearch(baiduFaceConfig);
//            return map;
//        }else {
//            return null;
//        }

        Map map = baiduAIFaceUtil.FaceSearch(baiduFaceConfig);
        return map;
    }

    public Map<String,Object> registerface(StringBuffer imagebase64) throws IOException {
        String substring = imagebase64.substring(imagebase64.indexOf(",")+1, imagebase64.length());
        baiduFaceConfig.setImgpath(substring);
        baiduFaceConfig.setImage_Type("BASE64");
        String uuid = UUID.randomUUID().toString().substring(0,9).replace("-","");
        baiduFaceConfig.setUserID(uuid);
        baiduFaceConfig.setGroupID("test");
        baiduFaceConfig.setLiveness_Control("NONE");
        baiduFaceConfig.setQuality_Control("NONE");
        Map check = baiduAIFaceUtil.FaceCheck(baiduFaceConfig);
//        if (check!=null && check.get("error_msg") == "SUCCESS"){
//            Map map = baiduAIFaceUtil.FaceRegistration(baiduFaceConfig);
//            return map;
//        }else {
//            return null;
//        }
        Map map = baiduAIFaceUtil.FaceRegistration(baiduFaceConfig);
        return map;


    }

}
