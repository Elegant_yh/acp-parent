package com.zx.auth.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class RedisUtil {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 将值存入redis 并设置60秒有效时间
     *
     * @param key
     * @param value
     */
    public void setTime(String key, String value) {
        redisTemplate.opsForValue().set(key, value, 60, TimeUnit.SECONDS);
    }

    /**
     * 根据key获取存入redis中的值
     *
     * @param key
     * @return
     */
    public String get(String key) {
        String value = "";
        Object object = redisTemplate.opsForValue().get(key);
        if (object != null) {
            value = object.toString();
        }
        return value;
    }

    /**
     * 判断key 是否存在
     * @param key
     * @return
     */
    public boolean hasKey(String key){
        return redisTemplate.hasKey(key);
    }

    /**
     * 删除key
     * @param key
     */
    public void deleteKey(String key){
        redisTemplate.delete(key);
    }

}
