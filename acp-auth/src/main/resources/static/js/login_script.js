new Vue({
    el: '#container',
    data() {
        return {
            // 登录名
            loginname: '',
            password:''
        }
    },
    methods: {
        login(){
            let formData = new FormData()
            formData.append('loginname', this.loginname)
            formData.append('password', this.password)
            axios.post('/login', formData).then(res => {
                if (res.data.code == 200){
                    this.$message({
                        message: res.data.message,
                        type: 'success'
                      });
                      window.location.href='/success'
                }else{
                    this.$message.error(res.data.message);
                }
            })
        }
    }
})