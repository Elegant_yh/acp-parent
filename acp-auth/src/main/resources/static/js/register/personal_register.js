new Vue({
    el: "#content",
    data() {
        return {
            // 身份证正面
            img_1: '',
            isShow_1: true,
            isImgShow_1: false,

            // 身份证反面
            img_2: '',
            isShow_2: true,
            isImgShow_2: false,

            // 表单信息
            PersonalUserCenterEntity: {
                // 昵称
                loginname: '',
                // 密码
                password: '',
                // 确认密码
                checkPass: '',
                // 手机号码
                mobile: '',
                // 用户名
                username: '',
                // 性别
                gender: '',
                // 身份证号
                identityNumber: '',
                // 用户地址
                address: '',
                // 民族
                minZu: '',
                // 生日
                birth: '',
                // 身份证正面照片
                identityFrontImage: '',
                // 身份证反面照片
                identityBackImage: '',
            },
            // 验证码
            code: '',
            // loading: false
        }
    },
    methods: {

        // 身份证正面
        uploadFile_1(file) {
            this.isShow_1 = false
            this.isImgShow_1 = true
            // this.loading = true
            let formData = new FormData()
            formData.append('file', file.file)
            formData.append('idCardSide', "front")
            axios.post('/third-party/idCard', formData).then(res => {
                console.log(res)
                if (res.data.code == 200) {
                    this.$notify({
                        title: '成功',
                        message: res.data.message,
                        type: 'success'
                    });
                    this.img_1 = res.data.data.imgPath
                    let words_result = JSON.parse(res.data.data.words_result)
                    this.PersonalUserCenterEntity.username = words_result.words_result.姓名.words
                    this.PersonalUserCenterEntity.gender = words_result.words_result.性别.words == '男' ? 0 : 1
                    this.PersonalUserCenterEntity.identityNumber = words_result.words_result.公民身份号码.words
                    this.PersonalUserCenterEntity.address = words_result.words_result.住址.words
                    this.PersonalUserCenterEntity.minZu = words_result.words_result.民族.words
                    this.PersonalUserCenterEntity.birth = words_result.words_result.出生.words
                    this.PersonalUserCenterEntity.identityFrontImage = res.data.data.imgPath

                } else {
                    this.isShow_1 = true
                    this.isImgShow_1 = false
                    this.$notify.error({
                        title: '错误',
                        message: res.data.message,
                    });
                }
            })
        },

        // 身份证反面
        uploadFile_2(file) {
            this.isImgShow_2 = true
            this.isShow_2 = false
            let formData = new FormData()
            formData.append('file', file.file)
            formData.append('idCardSide', "back")
            axios.post('/third-party/idCard', formData).then(res => {
                if (res.data.code == 200) {
                    this.$notify({
                        title: '成功',
                        message: res.data.message,
                        type: 'success'
                    });
                    this.img_2 = res.data.data.imgPath

                    // 身份证反面
                    this.PersonalUserCenterEntity.identityBackImage = res.data.data.imgPath
                } else {
                    this.isImgShow_2 = false
                    this.isShow_2 = true
                    this.$notify.error({
                        title: '错误',
                        message: res.data.message,
                    });
                }
            })
        },

        // 验证昵称是否存在
        loginNameExite() {
            axios.get('/register/loginNameExist?loginname=' + this.PersonalUserCenterEntity.loginname).then(res => {
                if (res.data.code != 200) {
                    this.$message({
                        message: res.data.message,
                        type: 'error'
                    });
                    this.PersonalUserCenterEntity.loginname = ''
                }
            })
        },

        // 提交注册信息
        onSubmit() {
            if (this.PersonalUserCenterEntity.loginname != null && this.PersonalUserCenterEntity.loginname != '')
                axios.post('/register/personal?code=' + this.code,
                    JSON.stringify(this.PersonalUserCenterEntity),
                    {
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8'
                        }
                    }
                ).then(res => {
                    if (res.data.code == 200) {
                        this.$message({
                            message: res.data.message,
                            type: 'success'
                        });

                        window.location.href = '/login';
                    } else {
                        this.$message({
                            message: res.data.message,
                            type: 'error'
                        });
                    }
                })
            else
                this.$message({
                    message: "昵称不能为空",
                    type: 'error'
                });
        }
    },
})

var countdown = 60;

function getCode(obj) {
    axios.post('/third-party/sendSMS?mobile=' + $("#mobile").val()).then(res => { });
    var time = setInterval(function () {
        if (countdown == 0) {
            obj.removeAttribute("disabled");
            obj.value = "获取验证码";
            countdown = 60;
            $("#btn").css("color", "#FFFFFF");
            clearInterval(time);
            return;
        } else {
            $("#btn").css("color", "#ccc");
            obj.setAttribute("disabled", true);
            obj.value = "重新发送(" + countdown + ")";
            countdown--;
        }
    }, 1000)
}