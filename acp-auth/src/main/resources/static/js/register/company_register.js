new Vue({
    el: "#content",
    data() {
        return {
            // 身份证正面
            img: '',
            isShow: true,
            isImgShow: false,

            // 表单信息
            UserCompanyCenterEntity: {
                // 昵称
                loginname: '',
                // 密码
                password: '',
                // 确认密码
                checkPass: '',
                // 手机号码
                phone: '',
                // 企业地址
                address: '',
                // 企业名/单位名称
                username: '',
                // 营业执照地址
                businessPicture: '',
                // 社会信用代码
                creditCode: '',
                // 经营范围
                businessScope: '',
                // 成立日期
                establishDate: '',
                // 注册资本
                registerCapital: '',
                // 证件编号
                certificcateNumber: '',
                // 类型
                companyType: '',
                // 企业法人
                legalPerson: '',
                // 有效期至
                regEndTime: '',
            },
            // 验证码
            code: ''
        }
    },
    methods: {

        // 营业执照识别
        uploadFile(file) {
            this.isShow = false
            this.isImgShow = true
            let formData = new FormData()
            formData.append('file', file.file)
            axios.post('/third-party/businessLicense', formData).then(res => {
                console.log(res)
                if (res.data.code == 200) {
                    this.$notify({
                        title: '成功',
                        message: res.data.message,
                        type: 'success'
                    });
                    this.img = res.data.data.imgPath
                    let words_result = JSON.parse(res.data.data.words_result)
                    this.UserCompanyCenterEntity.username = words_result.words_result.单位名称.words
                    this.UserCompanyCenterEntity.address = words_result.words_result.地址.words
                    this.UserCompanyCenterEntity.establishDate = (words_result.words_result.成立日期.words).replace('年', '-').replace('月', '-').replace('日', '')
                    this.UserCompanyCenterEntity.regEndTime = words_result.words_result.有效期.words
                    this.UserCompanyCenterEntity.legalPerson = words_result.words_result.法人.words
                    this.UserCompanyCenterEntity.registerCapital = words_result.words_result.注册资本.words
                    this.UserCompanyCenterEntity.creditCode = words_result.words_result.社会信用代码.words
                    this.UserCompanyCenterEntity.companyType = words_result.words_result.类型.words
                    this.UserCompanyCenterEntity.businessScope = words_result.words_result.经营范围.words
                    this.UserCompanyCenterEntity.certificcateNumber = words_result.words_result.证件编号.words
                    this.UserCompanyCenterEntity.businessPicture = res.data.data.imgPath

                } else {
                    this.isShow = true
                    this.isImgShow = false
                    this.$notify.error({
                        title: '错误',
                        message: res.data.message,
                    });
                }
            })
        },

        // 验证昵称是否存在
        loginNameExite() {
            axios.get('/register/loginNameExist?loginname=' + this.UserCompanyCenterEntity.loginname).then(res => {
                if (res.data.code != 200) {
                    this.$message({
                        message: res.data.message,
                        type: 'error'
                    });
                    this.UserCompanyCenterEntity.loginname = ''
                }
            })
        },

        // 提交注册信息
        onSubmit() {
            if (this.UserCompanyCenterEntity.loginname != null && this.UserCompanyCenterEntity.loginname != '')
                axios.post('/register/company?code=' + this.code,
                    JSON.stringify(this.UserCompanyCenterEntity),
                    {
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8'
                        }
                    }
                ).then(res => {
                    console.log(res)
                    if (res.data.code == 200) {
                        this.$message({
                            message: res.data.message,
                            type: 'success'
                        });
                        window.location.href = '/login';
                    } else {
                        this.$message({
                            message: res.data.message,
                            type: 'error'
                        });
                    }
                })
            else
                this.$message({
                    message: "昵称不能为空",
                    type: 'error'
                });
        }
    },
})

var countdown = 60;

function getCode(obj) {
    axios.post('/third-party/sendSMS?mobile=' + $("#mobile").val()).then(res => { });
    var time = setInterval(function () {
        if (countdown == 0) {
            obj.removeAttribute("disabled");
            obj.value = "获取验证码";
            countdown = 60;
            $("#btn").css("color", "#FFFFFF");
            clearInterval(time);
            return;
        } else {
            $("#btn").css("color", "#ccc");
            obj.setAttribute("disabled", true);
            obj.value = "重新发送(" + countdown + ")";
            countdown--;
        }
    }, 1000)
}